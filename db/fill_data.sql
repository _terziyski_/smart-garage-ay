INSERT INTO smart_garage.brands (brand_id, name) VALUES (13, 'Aston-Martin');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (2, 'Audi');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (8, 'Bentley');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (3, 'BMW');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (20, 'Chrysler');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (21, 'Dodge');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (6, 'Ferrari');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (12, 'Fiat');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (19, 'Honda');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (15, 'Lada');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (7, 'Lamborghini');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (16, 'Lincoln');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (17, 'Maserati');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (5, 'Mazda');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (1, 'Mercedes-Benz');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (9, 'Opel');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (18, 'Renault');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (10, 'SAAB');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (11, 'Seat');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (14, 'Skoda');
INSERT INTO smart_garage.brands (brand_id, name) VALUES (4, 'Toyota');

INSERT INTO smart_garage.models (model_id, brand_id, name) VALUES (4, 3, '335i');
INSERT INTO smart_garage.models (model_id, brand_id, name) VALUES (5, 3, '535d');
INSERT INTO smart_garage.models (model_id, brand_id, name) VALUES (6, 2, 'A3');
INSERT INTO smart_garage.models (model_id, brand_id, name) VALUES (7, 2, 'A4');
INSERT INTO smart_garage.models (model_id, brand_id, name) VALUES (8, 2, 'A5');
INSERT INTO smart_garage.models (model_id, brand_id, name) VALUES (9, 2, 'A6');
INSERT INTO smart_garage.models (model_id, brand_id, name) VALUES (3, 1, 'C63');
INSERT INTO smart_garage.models (model_id, brand_id, name) VALUES (2, 1, 'E63');
INSERT INTO smart_garage.models (model_id, brand_id, name) VALUES (1, 1, 'GT63s');
INSERT INTO smart_garage.models (model_id, brand_id, name) VALUES (10, 2, 'Q7');
INSERT INTO smart_garage.models (model_id, brand_id, name) VALUES (11, 2, 'S8');


INSERT INTO smart_garage.services (service_id, type, price, description, icon) VALUES (1, 'Engine Repair', 350, 'Lorem Ipsum has been the industry''s standard dummy text ever.', 'flaticon-car-1');
INSERT INTO smart_garage.services (service_id, type, price, description, icon) VALUES (2, 'Tires Replacement', 25, 'Lorem Ipsum has been the industry''s standard dummy text ever.', 'flaticon-tire');
INSERT INTO smart_garage.services (service_id, type, price, description, icon) VALUES (3, 'Diagnostic', 20, 'Lorem Ipsum has been the industry''s standard dummy text ever.', 'fi flaticon-car-repair');
INSERT INTO smart_garage.services (service_id, type, price, description, icon) VALUES (4, 'Transmission', 200, 'Lorem Ipsum has been the industry''s standard dummy text ever.', 'flaticon-gear-stick');
INSERT INTO smart_garage.services (service_id, type, price, description, icon) VALUES (5, 'Breaks', 70, 'Lorem Ipsum has been the industry''s standard dummy text ever.', 'flaticon-break');
INSERT INTO smart_garage.services (service_id, type, price, description, icon) VALUES (6, 'Batteries', 90, 'Lorem Ipsum has been the industry''s standard dummy text ever.', 'fi flaticon-battery');
INSERT INTO smart_garage.services (service_id, type, price, description, icon) VALUES (7, 'Oil change', 130, 'Lorem Ipsum has been the industry''s standard dummy text ever.', 'flaticon-oil');
INSERT INTO smart_garage.services (service_id, type, price, description, icon) VALUES (8, 'Filters change', 40, 'Lorem Ipsum has been the industry''s standard dummy text ever.', 'flaticon-air-filter');

INSERT INTO smart_garage.roles (role_id, type) VALUES (1, 'customer');
INSERT INTO smart_garage.roles (role_id, type) VALUES (2, 'employee');

INSERT INTO smart_garage.credentials (credentials_id, email, password) VALUES (1, 'alex@gmail.com', 'pass1');
INSERT INTO smart_garage.credentials (credentials_id, email, password) VALUES (2, 'july@gmail.com', 'pass1');
INSERT INTO smart_garage.credentials (credentials_id, email, password) VALUES (3, 'valio@abv.bg', 'pass2');
INSERT INTO smart_garage.credentials (credentials_id, email, password) VALUES (4, 'gosho@abv.bg', 'pass2');
INSERT INTO smart_garage.credentials (credentials_id, email, password) VALUES (5, 'rado@abv.bg', 'pass2');
INSERT INTO smart_garage.credentials (credentials_id, email, password) VALUES (6, 'pesho@abv.bg', 'pass2');
INSERT INTO smart_garage.credentials (credentials_id, email, password) VALUES (7, 'misho@abv.bg', 'pass2');
INSERT INTO smart_garage.credentials (credentials_id, email, password) VALUES (8, 'juli00913@gmail.com', 'pass2');

INSERT INTO smart_garage.credentials_roles (credential_id, role_id) VALUES (1, 2);
INSERT INTO smart_garage.credentials_roles (credential_id, role_id) VALUES (2, 2);
INSERT INTO smart_garage.credentials_roles (credential_id, role_id) VALUES (3, 1);
INSERT INTO smart_garage.credentials_roles (credential_id, role_id) VALUES (4, 1);
INSERT INTO smart_garage.credentials_roles (credential_id, role_id) VALUES (5, 1);
INSERT INTO smart_garage.credentials_roles (credential_id, role_id) VALUES (6, 1);
INSERT INTO smart_garage.credentials_roles (credential_id, role_id) VALUES (7, 1);
INSERT INTO smart_garage.credentials_roles (credential_id, role_id) VALUES (8, 1);

INSERT INTO smart_garage.customers (customer_id, first_name, last_name, phone, credentials_id) VALUES (1, 'Valio', 'Ivanov', '0887878981', 3);
INSERT INTO smart_garage.customers (customer_id, first_name, last_name, phone, credentials_id) VALUES (2, 'George', 'Iliev', '0887456325', 4);
INSERT INTO smart_garage.customers (customer_id, first_name, last_name, phone, credentials_id) VALUES (3, 'Radoslav', 'Slavchev', '0889658745', 5);
INSERT INTO smart_garage.customers (customer_id, first_name, last_name, phone, credentials_id) VALUES (4, 'Peter', 'Nikolov', '0884521547', 6);
INSERT INTO smart_garage.customers (customer_id, first_name, last_name, phone, credentials_id) VALUES (5, 'Mihail', 'Tonchev', '0887987897', 7);
INSERT INTO smart_garage.customers (customer_id, first_name, last_name, phone, credentials_id) VALUES (6, 'Yuliyan', 'Slavchev', '0882028747', 8);

INSERT INTO smart_garage.employees (employee_id, credentials_id) VALUES (1, 1);
INSERT INTO smart_garage.employees (employee_id, credentials_id) VALUES (2, 2);

INSERT INTO smart_garage.vehicles (vehicle_id, customer_id, model_id, year, vin, license_plate) VALUES (5, 1, 3, 2018, '1C4NJPBA6CD722474', 'CA4563KH');
INSERT INTO smart_garage.vehicles (vehicle_id, customer_id, model_id, year, vin, license_plate) VALUES (6, 2, 4, 2016, 'WBAVC93598K041359', 'E7654BP');
INSERT INTO smart_garage.vehicles (vehicle_id, customer_id, model_id, year, vin, license_plate) VALUES (7, 3, 1, 2017, 'JN1HZ14S9GX141292', 'B3249HK');
INSERT INTO smart_garage.vehicles (vehicle_id, customer_id, model_id, year, vin, license_plate) VALUES (8, 4, 2, 2007, '1FTWW33P44EC94708', 'CT9080TC');
INSERT INTO smart_garage.vehicles (vehicle_id, customer_id, model_id, year, vin, license_plate) VALUES (9, 5, 5, 2020, 'KL8CA6S96EC529606', 'A3487CK');
INSERT INTO smart_garage.vehicles (vehicle_id, customer_id, model_id, year, vin, license_plate) VALUES (10, 6, 3, 1998, 'LO7A6S96EC525865', 'X3453RT');

INSERT INTO smart_garage.statuses (status_id, type) VALUES (1, 'Not Started');
INSERT INTO smart_garage.statuses (status_id, type) VALUES (2, 'In Progress');
INSERT INTO smart_garage.statuses (status_id, type) VALUES (3, 'Ready for Pickup');
INSERT INTO smart_garage.statuses (status_id, type) VALUES (4, 'Picked up');

INSERT INTO smart_garage.visits (visit_id, vehicle_id, total_price, date, status_id) VALUES (2, 5, 350, '2021-04-01', 4);
INSERT INTO smart_garage.visits (visit_id, vehicle_id, total_price, date, status_id) VALUES (3, 6, 95, '2021-04-09', 4);
INSERT INTO smart_garage.visits (visit_id, vehicle_id, total_price, date, status_id) VALUES (4, 7, 90, '2021-03-18', 3);
INSERT INTO smart_garage.visits (visit_id, vehicle_id, total_price, date, status_id) VALUES (5, 8, 220, '2021-03-26', 1);
INSERT INTO smart_garage.visits (visit_id, vehicle_id, total_price, date, status_id) VALUES (6, 9, 170, '2021-03-30', 2);

INSERT INTO smart_garage.visits_services (visit_id, service_id) VALUES (2, 1);
INSERT INTO smart_garage.visits_services (visit_id, service_id) VALUES (3, 2);
INSERT INTO smart_garage.visits_services (visit_id, service_id) VALUES (3, 5);
INSERT INTO smart_garage.visits_services (visit_id, service_id) VALUES (4, 6);
INSERT INTO smart_garage.visits_services (visit_id, service_id) VALUES (5, 3);
INSERT INTO smart_garage.visits_services (visit_id, service_id) VALUES (5, 4);
INSERT INTO smart_garage.visits_services (visit_id, service_id) VALUES (6, 7);
INSERT INTO smart_garage.visits_services (visit_id, service_id) VALUES (6, 8);

INSERT INTO smart_garage.currencies (currency_id, type) VALUES (1, 'BGN');
INSERT INTO smart_garage.currencies (currency_id, type) VALUES (2, 'EUR');
INSERT INTO smart_garage.currencies (currency_id, type) VALUES (3, 'USD');
INSERT INTO smart_garage.currencies (currency_id, type) VALUES (4, 'GBP');
