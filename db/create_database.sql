CREATE DATABASE IF NOT EXISTS `smart_garage`;
USE smart_garage;

create or replace table brands
(
    brand_id int auto_increment
        primary key,
    name     varchar(50) not null,
    constraint brand_name_uindex
        unique (name)
);

create or replace table models
(
    model_id int auto_increment
        primary key,
    brand_id int         not null,
    name     varchar(50) not null,
    constraint model_name_uindex
        unique (name),
    constraint models_brands_fk
        foreign key (brand_id) references brands (brand_id)
);

create or replace table roles
(
    role_id int auto_increment
        primary key,
    type    varchar(50) not null,
    constraint roles_type_uindex
        unique (type)
);

create or replace table credentials
(
    credentials_id int auto_increment
        primary key,
    email          varchar(200) not null,
    password       varchar(200) not null,
    constraint credentials_email_uindex
        unique (email)
);

create table credentials_roles
(
    credential_id int not null,
    role_id       int not null,
    constraint credentials_roles_credentials_fk
        foreign key (credential_id) references credentials (credentials_id),
    constraint credentials_roles_roles_fk
        foreign key (role_id) references roles (role_id)
);

create or replace table customers
(
    customer_id    int auto_increment
        primary key,
    first_name     varchar(20) not null,
    last_name      varchar(20) not null,
    phone          varchar(10) null,
    credentials_id int         not null,
    constraint customers_credentials_fk
        foreign key (credentials_id) references credentials (credentials_id)
);

create or replace table employees
(
    employee_id    int auto_increment
        primary key,
    credentials_id int not null,
    constraint employees_credentials_fk
        foreign key (credentials_id) references credentials (credentials_id)
);

create or replace table vehicles
(
    vehicle_id    int auto_increment
        primary key,
    customer_id   int         not null,
    model_id      int         not null,
    year          int         not null,
    vin           varchar(17) not null,
    license_plate varchar(8)  not null,
    constraint vehicles_license_plate_uindex
        unique (license_plate),
    constraint vehicles_vin_uindex
        unique (vin),
    constraint vehicles_models_fk
        foreign key (model_id) references models (model_id),
    constraint vehicles_customers_fk
        foreign key (customer_id) references customers (customer_id)
);

create or replace table statuses
(
    status_id int auto_increment
        primary key,
    type      varchar(50) not null,
    constraint status_type_uindex
        unique (type)
);

create or replace table visits
(
    visit_id    int auto_increment
        primary key,
    vehicle_id  int    not null,
    total_price double not null,
    date        date   not null,
    status_id   int    not null,
    constraint visits_vehicles_fk
        foreign key (vehicle_id) references vehicles (vehicle_id),
    constraint visits_statuses_fk
        foreign key (status_id) references statuses (status_id)
);

create or replace table services
(
    service_id int auto_increment
        primary key,
    type varchar(50) not null,
    price double not null,
    description varchar(500) not null,
    icon varchar(100) not null
);

create or replace table currencies
(
    currency_id int auto_increment
        primary key,
    type        varchar(10) not null
);

create or replace table visits_services
(
    visit_id   int not null,
    service_id int not null,
    constraint visits_services_services_fk
        foreign key (service_id) references services (service_id),
    constraint visits_services_visits_fk
        foreign key (visit_id) references visits (visit_id)
);




