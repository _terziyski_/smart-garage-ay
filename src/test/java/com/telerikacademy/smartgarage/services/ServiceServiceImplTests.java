package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Customer;
import com.telerikacademy.smartgarage.models.Service;
import com.telerikacademy.smartgarage.models.dtos.ServiceFilterParameters;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.smartgarage.helpers.CredentialsHelper.createMockCredentialsCustomer;
import static com.telerikacademy.smartgarage.helpers.CredentialsHelper.createMockCredentialsEmployee;
import static com.telerikacademy.smartgarage.helpers.CustomerHelper.createMockCustomer;
import static com.telerikacademy.smartgarage.helpers.ServiceHelper.createMockService;

@ExtendWith(MockitoExtension.class)
public class ServiceServiceImplTests {

    @Mock
    ServiceRepository serviceRepository;

    @InjectMocks
    ServiceServiceImpl serviceService;

    @Test
    public void getAll_Should_ReturnServices_When_MatchExists(){
        List<Service> services = new ArrayList<>();
        services.add(createMockService());
        services.add(createMockService());

        Mockito.when(serviceRepository.getAll())
                .thenReturn(services);

        List<Service> result = serviceService.getAll();

        Assertions.assertEquals(services,result);
    }

    @Test
    public void getById_Should_ReturnServices_When_MatchExists(){

        Mockito.when(serviceRepository.getById(1))
                .thenReturn(createMockService());

        Service service = serviceService.getById(1);

        Assertions.assertEquals(1,service.getId());
        Assertions.assertEquals("Mock Service",service.getType());
        Assertions.assertEquals("Mock Description",service.getDescription());
        Assertions.assertEquals("mock icon",service.getIcon());
        Assertions.assertEquals(69.69,service.getPrice());
    }

    @Test
    public void getById_Should_Throw_When_MatchDoesNotExists(){
        Mockito.when(serviceRepository.getById(1))
                .thenThrow(new EntityNotFoundException("Service",1));

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> serviceService.getById(1));
    }

    @Test
    public void filter_Should_CallRepository(){
        ServiceFilterParameters sfp = new ServiceFilterParameters();

        serviceService.filter(sfp);

        Mockito.verify(serviceRepository,Mockito.times(1)).filter(sfp);
    }

    @Test
    public void create_Should_CallRepository_When_UserIsEmployee() {
        Credentials credentials = createMockCredentialsEmployee();
        Service service = createMockService();

        serviceService.create(service, credentials);

        Mockito.verify(serviceRepository, Mockito.times(1))
                .create(service);
    }

    @Test
    public void create_Should_Throw_When_UserIsCustomer() {
        Credentials credentials = createMockCredentialsCustomer();
        Service service = createMockService();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> serviceService.create(service, credentials));
    }

    @Test
    public void update_Should_CallRepository_When_UserIsEmployee() {
        Credentials credentials = createMockCredentialsEmployee();
        Service service = createMockService();

        serviceService.update(service, credentials);

        Mockito.verify(serviceRepository, Mockito.times(1))
                .update(service);
    }

    @Test
    public void update_Should_Throw_When_UserIsCustomer() {
        Credentials credentials = createMockCredentialsCustomer();
        Service service = createMockService();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> serviceService.update(service, credentials));
    }

    @Test
    public void delete_Should_CallRepository_When_UserIsEmployee() {
        Credentials credentials = createMockCredentialsEmployee();
        Service service = createMockService();

        serviceService.delete(service, credentials);

        Mockito.verify(serviceRepository, Mockito.times(1))
                .delete(service);
    }

    @Test
    public void delete_Should_Throw_When_UserIsCustomer() {
        Credentials credentials = createMockCredentialsCustomer();
        Service service = createMockService();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> serviceService.delete(service, credentials));
    }
}
