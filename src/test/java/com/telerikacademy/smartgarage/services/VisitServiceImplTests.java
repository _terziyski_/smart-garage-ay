package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.helpers.CustomerHelper;
import com.telerikacademy.smartgarage.helpers.VisitHelper;
import com.telerikacademy.smartgarage.models.*;
import com.telerikacademy.smartgarage.models.dtos.VisitFilterParameters;
import com.telerikacademy.smartgarage.repositories.contracts.CustomerRepository;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleRepository;
import com.telerikacademy.smartgarage.repositories.contracts.VisitRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.telerikacademy.smartgarage.helpers.CredentialsHelper.createMockCredentialsCustomer;
import static com.telerikacademy.smartgarage.helpers.CredentialsHelper.createMockCredentialsEmployee;
import static com.telerikacademy.smartgarage.helpers.CustomerHelper.createMockCustomer;
import static com.telerikacademy.smartgarage.helpers.ServiceHelper.createMockService;
import static com.telerikacademy.smartgarage.helpers.VehicleHelper.createMockVehicle;
import static com.telerikacademy.smartgarage.helpers.VehicleHelper.createVfp;
import static com.telerikacademy.smartgarage.helpers.VisitHelper.createMockVisit;

@ExtendWith(MockitoExtension.class)
public class VisitServiceImplTests {

    @Mock
    VisitRepository visitRepository;

    @Mock
    CustomerRepository customerRepository;

    @InjectMocks
    VisitServiceImpl visitService;

    @Test
    public void getAll_Should_ReturnVisits_When_MatchExists() {
        List<Visit> visits = new ArrayList<>();
        Visit visit = createMockVisit();
        visits.add(visit);

        Mockito.when(visitRepository.getAll())
                .thenReturn(visits);

        List<Visit> result = visitService.getAll(createMockCredentialsEmployee());

        Assertions.assertEquals(visits, result);
    }

    @Test
    public void getAll_Should_Throw_When_UserIsNotEmployee() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> visitService.getAll(createMockCredentialsCustomer()));
    }

    @Test
    public void getNewestVisitDate_Should_ReturnDate() {
        Date date = Date.valueOf(LocalDate.now());

        Mockito.when(visitRepository.getNewestVisitDate())
                .thenReturn(date);

        Date result = visitService.getNewestVisitDate(createMockCredentialsEmployee());

        Assertions.assertEquals(date, result);
    }

    @Test
    public void getNewestVisitDateCustomer_Should_ReturnDate() {
        Date date = Date.valueOf(LocalDate.now());

        Mockito.when(visitRepository.getNewestVisitDateCustomer(1))
                .thenReturn(date);

        Date result = visitService.getNewestVisitDate(createMockCredentialsCustomer());

        Assertions.assertEquals(date, result);
    }

    @Test
    public void getOldestVisitDate_Should_ReturnDate() {
        Date date = Date.valueOf(LocalDate.now());

        Mockito.when(visitRepository.getOldestVisitDate())
                .thenReturn(date);

        Date result = visitService.getOldestVisitDate(createMockCredentialsEmployee());

        Assertions.assertEquals(date, result);
    }

    @Test
    public void getOldestVisitDateCustomer_Should_ReturnDate() {
        Date date = Date.valueOf(LocalDate.now());

        Mockito.when(visitRepository.getOldestVisitDateCustomer(1))
                .thenReturn(date);

        Date result = visitService.getOldestVisitDate(createMockCredentialsCustomer());

        Assertions.assertEquals(date, result);
    }

    @Test
    public void getById_Should_ReturnVisits_When_MatchExists() {
        Visit visit = createMockVisit();
        Credentials credentials = createMockCredentialsEmployee();
        credentials.setId(2);

        Mockito.when(visitRepository.getById(1))
                .thenReturn(visit);

        Visit result = visitService.getById(1, credentials);

        Assertions.assertEquals(visit, result);
    }

    @Test
    public void getByOwnerId_Should_ReturnVisits_When_MatchExists() {
        Visit visit = createMockVisit();

        Mockito.when(visitRepository.getById(1))
                .thenReturn(visit);

        Visit result = visitService.getById(1, createMockCredentialsCustomer());

        Assertions.assertEquals(visit, result);
    }

    @Test
    public void getByCustomerId_Should_Throw_When_UserIsNotEmployee() {
        Visit visit = createMockVisit();

        Mockito.when(visitRepository.getById(1))
                .thenReturn(visit);

        Credentials credentials = createMockCredentialsCustomer();
        credentials.setId(2);
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> visitService.getById(1, credentials));
    }

    @Test
    public void getAllByCustomerId_Should_ReturnVisits_When_MatchExists() {
        Customer customer = createMockCustomer();
        Credentials credentials = createMockCredentialsCustomer();

        Mockito.when(customerRepository.getById(1))
                .thenReturn(customer);

        visitService.getAllByCustomerId(1, credentials);

        Mockito.verify(visitRepository, Mockito.times(1)).getAllByCustomerId(1);
    }

    @Test
    public void getAllByCustomerId_Should_ReturnVisits_When_UserIsEmployee() {
        Customer customer = createMockCustomer();
        Credentials credentials = createMockCredentialsEmployee();
        credentials.setId(2);

        Mockito.when(customerRepository.getById(1))
                .thenReturn(customer);

        visitService.getAllByCustomerId(1, credentials);

        Mockito.verify(visitRepository, Mockito.times(1)).getAllByCustomerId(1);
    }

    @Test
    public void getAllByCredentialsId_Should_ReturnVisits_When_MatchExists() {
        List<Visit> visits = new ArrayList<>();
        visits.add(createMockVisit());

        Mockito.when(visitRepository.getAllByCredentialsId(1))
                .thenReturn(visits);


        List<Visit> result = visitService.getAllByCredentialsId(1);
        Assertions.assertEquals(visits, result);
    }

    @Test
    public void filter_Should_ReturnVisits_When_MatchExists() {
        VisitFilterParameters vfp = new VisitFilterParameters();
        Credentials credentials = createMockCredentialsEmployee();

        visitService.filter(vfp, credentials);

        Mockito.verify(visitRepository, Mockito.times(1))
                .filter(vfp, credentials);
    }

    @Test
    public void create_Should_throw_When_CustomerCredentials() {
        // Arrange
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> visitService.create(createMockVisit(), createMockCredentialsCustomer()));
    }

    @Test
    public void create_Should_CallRepository_When_UserIsEmployee() {
        Credentials credentials = createMockCredentialsEmployee();
        Visit visit = createMockVisit();

        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> visitService.create(visit, credentials));
    }

    @Test
    public void update_Should_CallRepository_When_UserIsEmployee() {
        Credentials credentials = createMockCredentialsEmployee();
        Visit visit = createMockVisit();

        visitService.update(visit, credentials);

        Mockito.verify(visitRepository, Mockito.times(1))
                .update(visit);
    }

    @Test
    public void update_Should_Throw_When_UserIsCustomer() {
        Credentials credentials = createMockCredentialsCustomer();
        Visit visit = createMockVisit();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> visitService.update(visit, credentials));
    }

    @Test
    public void delete_Should_CallRepository_When_UserIsEmployee() {
        Credentials credentials = createMockCredentialsEmployee();
        Visit visit = createMockVisit();

        visitService.delete(visit, credentials);

        Mockito.verify(visitRepository, Mockito.times(1))
                .delete(visit);
    }

    @Test
    public void delete_Should_Throw_When_UserIsCustomer() {
        Credentials credentials = createMockCredentialsCustomer();
        Visit visit = createMockVisit();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> visitService.delete(visit, credentials));
    }

    @Test
    public void generateReport_Should_Throw_When_Called() {
        Visit visit = createMockVisit();
        String email = "mockmail@abv.bg";

        Assertions.assertThrows(NullPointerException.class,
                () -> visitService.generateReport(visit, "BGN", email));
    }

}
