package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Currency;
import com.telerikacademy.smartgarage.models.Status;
import com.telerikacademy.smartgarage.repositories.contracts.StatusRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.smartgarage.helpers.StatusHelper.createMockStatus;

@ExtendWith(MockitoExtension.class)
public class StatusServiceImplTests {

    @Mock
    StatusRepository statusRepository;

    @InjectMocks
    StatusServiceImpl statusService;

    @Test
    public void getAll_Should_ReturnStatuses_When_MatchExists(){
        List<Status> statuses = new ArrayList<>();
        statuses.add(createMockStatus());
        statuses.add(createMockStatus());

        Mockito.when(statusRepository.getAll())
                .thenReturn(statuses);

        List<Status> result = statusService.getAll();

        Assertions.assertEquals(statuses,result);
    }

    @Test
    public void getById_Should_ReturnStatus_When_MatchExists(){

        Mockito.when(statusRepository.getById(1))
                .thenReturn(createMockStatus());

        Status status = statusService.getById(1);

        Assertions.assertEquals(1,status.getId());
        Assertions.assertEquals("Mock Status",status.getType());
    }

    @Test
    public void getById_Should_Throw_When_MatchDoesNotExists(){
        Mockito.when(statusRepository.getById(1))
                .thenThrow(new EntityNotFoundException("Status",1));

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> statusService.getById(1));
    }
}
