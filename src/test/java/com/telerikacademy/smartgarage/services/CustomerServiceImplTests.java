package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Customer;
import com.telerikacademy.smartgarage.models.dtos.CustomerFilterParameters;
import com.telerikacademy.smartgarage.repositories.contracts.CredentialsRepository;
import com.telerikacademy.smartgarage.repositories.contracts.CustomerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.smartgarage.helpers.CredentialsHelper.createMockCredentialsCustomer;
import static com.telerikacademy.smartgarage.helpers.CredentialsHelper.createMockCredentialsEmployee;
import static com.telerikacademy.smartgarage.helpers.CustomerHelper.createMockCustomer;
import static com.telerikacademy.smartgarage.helpers.ValidationHelper.checkIsEmployee;
import static com.telerikacademy.smartgarage.helpers.VehicleHelper.createMockVehicle;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceImplTests {

    @Mock
    CustomerRepository customerRepository;

    @Mock
    CredentialsRepository credentialsRepository;

    @Mock
    JavaMailSender sender;

    @InjectMocks
    CustomerServiceImpl customerService;


    @Test
    public void getAll_Should_ReturnCustomers_When_UserIsEmployee() {
        Credentials credentials = createMockCredentialsEmployee();
        List<Customer> customers = new ArrayList<>();
        customers.add(createMockCustomer());
        customers.add(createMockCustomer());

        Mockito.when(customerRepository.getAll())
                .thenReturn(customers);

        List<Customer> result = customerService.getAll(credentials);

        Assertions.assertEquals(customers, result);
    }

    @Test
    public void getAll_Should_Throw_When_UserIsCustomer() {
        Credentials credentials = createMockCredentialsCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> customerService.getAll(credentials));
    }

    @Test
    public void getAllSorted_Should_ReturnCustomersSorted() {
        List<Customer> customers = new ArrayList<>();
        customers.add(createMockCustomer());
        customers.add(createMockCustomer());

        Mockito.when(customerRepository.getAllSorted("name", "asc"))
                .thenReturn(customers);

        List<Customer> result = customerService.getAllSorted("name", "asc");

        Assertions.assertEquals(customers, result);
    }

    @Test
    public void getById_Should_ReturnCustomer_When_UserIsEmployee() {
        Credentials credentials = createMockCredentialsEmployee();
        Mockito.when(customerRepository.getById(1))
                .thenReturn(createMockCustomer());

        Customer customer = customerService.getById(1, credentials);

        Assertions.assertEquals(1, customer.getId());
        Assertions.assertEquals("Mock", customer.getFirstName());
        Assertions.assertEquals("Mockov", customer.getLastName());
        Assertions.assertEquals("0883483939", customer.getPhone());
    }

    @Test
    public void getById_Should_Throw_When_UserIsCustomer() {
        Credentials credentials = createMockCredentialsCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> customerService.getById(1, credentials));
    }

    @Test
    public void getById_Should_Throw_When_MatchDoesNotExists() {
        Credentials credentials = createMockCredentialsEmployee();
        Mockito.when(customerRepository.getById(1))
                .thenThrow(new EntityNotFoundException("Customer", 1));

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> customerService.getById(1, credentials));
    }

    @Test
    public void getByEmail_Should_ReturnCustomer_When_UserIsEmployee() {
        Credentials credentials = createMockCredentialsEmployee();
        Mockito.when(customerRepository.getByEmail("mockCustomer@abv.bg"))
                .thenReturn(createMockCustomer());

        Customer customer = customerService.getByEmail("mockCustomer@abv.bg", credentials);

        Assertions.assertEquals(1, credentials.getId());
        Assertions.assertEquals("mockEmployee@abv.bg", credentials.getEmail());
        Assertions.assertEquals("Mock", customer.getFirstName());
        Assertions.assertEquals("Mockov", customer.getLastName());
        Assertions.assertEquals("0883483939", customer.getPhone());
    }

    @Test
    public void getByEmail_Should_Throw_When_MatchDoesNotExists() {
        Credentials credentials = createMockCredentialsEmployee();
        Mockito.when(customerRepository.getByEmail("mockCustomer@abv.bg"))
                .thenThrow(new EntityNotFoundException("Customer", "email", "mockCustomer@abv.bg"));

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> customerService.getByEmail("mockCustomer@abv.bg", credentials));
    }

    @Test
    public void getByEmail_Should_Throw_When_UserIsCustomer() {
        Credentials credentials = createMockCredentialsCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> customerService.getByEmail("mockEmail@abv.bg", credentials));
    }

    @Test
    public void getByEmail_Should_ReturnCustomer_When_EmailIsEqual() {
        Credentials credentials = createMockCredentialsCustomer();

        customerService.getByEmail(credentials.getEmail(), credentials);

        Mockito.verify(customerRepository, Mockito.times(1))
                .getByEmail(credentials.getEmail());
    }

    @Test
    public void create_Should_CallRepository_When_UserIsEmployee() throws Exception {
        Credentials credentials = createMockCredentialsEmployee();
        Customer customer = createMockCustomer();


        Mockito.when(credentialsRepository.getByEmail(customer.getCredentials().getEmail()))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(NullPointerException.class,
                () -> customerService.create(customer, credentials));

        Mockito.verify(customerRepository, Mockito.times(1))
                .create(customer);

    }

    @Test
    public void create_Should_Throw_When_UserIsCustomer() {
        Credentials credentials = createMockCredentialsCustomer();
        Customer customer = createMockCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> customerService.create(customer, credentials));
    }

    @Test
    public void update_Should_CallRepository_When_EmailIsEqual() {
        Credentials credentials = createMockCredentialsCustomer();
        Customer customer = createMockCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> customerService.create(customer, credentials));

        Assertions.assertDoesNotThrow(
                () -> customerRepository.create(customer));
    }

//    @Test
//    public void update_Should_CallRepository_When_UserIsEmployee() {
//        Credentials credentials = createMockCredentialsEmployee();
//        Customer customer = createMockCustomer();
//
//
//        Mockito.when(credentialsRepository.getByEmail(customer.getCredentials().getEmail()))
//                .thenThrow(EntityNotFoundException.class);
//
//        customerService.update(customer,credentials);
//
//        Mockito.verify(customerRepository, Mockito.times(1))
//                .update(customer);
//    }

    @Test
    public void update_Should_Throw_When_CustomerWithOtherEmail() {
        Credentials credentials = createMockCredentialsCustomer();
        credentials.setEmail("test@gmail.com");
        Customer customer = createMockCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> customerService.update(customer, credentials));
    }

//    @Test
//    public void update_Should_Throw_When_DuplicateExist(){
//
//    }

    @Test
    public void delete_Should_CallRepository_When_UserIsEmployee() {
        Credentials credentials = createMockCredentialsEmployee();
        Customer customer = createMockCustomer();

        customerService.delete(customer, credentials);

        Mockito.verify(customerRepository, Mockito.times(1))
                .delete(customer);
    }

    @Test
    public void delete_Should_Throw_When_UserIsCustomer() {
        Credentials credentials = createMockCredentialsCustomer();
        Customer customer = createMockCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> customerService.delete(customer, credentials));
    }

    @Test
    public void filter_Should_CallRepository_When_UserIsEmployee() {
        Credentials credentials = createMockCredentialsEmployee();
        CustomerFilterParameters cfp = new CustomerFilterParameters();

        customerService.filter(cfp, credentials);

        Mockito.verify(customerRepository, Mockito.times(1))
                .filter(cfp);
    }

    @Test
    public void filter_Should_Throw_When_UserIsCustomer() {
        Credentials credentials = createMockCredentialsCustomer();
        CustomerFilterParameters cfp = new CustomerFilterParameters();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> customerService.filter(cfp, credentials));
    }

    @Test
    public void sender_Should_CallSend() {
        MimeMessage message = sender.createMimeMessage();

        sender.send(message);

        Mockito.verify(sender, Mockito.times(1)).send(message);
    }
}
