package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Currency;
import com.telerikacademy.smartgarage.repositories.contracts.CurrencyRepository;
import com.telerikacademy.smartgarage.services.contracts.CurrencyService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class CurrencyServiceImplTests {

    @Mock
    CurrencyRepository currencyRepository;

    @InjectMocks
    CurrencyServiceImpl currencyService;

    @Test
    public void getAll_Should_ReturnCurrencies_When_MatchExists(){
        List<Currency> currencies = new ArrayList<>();
        currencies.add(new Currency(1,"BGN"));
        currencies.add(new Currency(2,"EUR"));

        Mockito.when(currencyRepository.getAll())
                .thenReturn(currencies);

        List<Currency> result = currencyService.getAll();

        Assertions.assertEquals(currencies,result);
    }

    @Test
    public void getById_Should_ReturnCurrency_When_MatchExists(){

        Mockito.when(currencyRepository.getById(1))
                .thenReturn(new Currency(1,"BGN"));

        Currency currency = currencyService.getById(1);

        Assertions.assertEquals(1,currency.getId());
        Assertions.assertEquals("BGN",currency.getType());
    }

    @Test
    public void getById_Should_Throw_When_MatchDoesNotExists(){
        Mockito.when(currencyRepository.getById(1))
                .thenThrow(new EntityNotFoundException("Currency",1));

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> currencyService.getById(1));
    }

    @Test
    public void getByType_Should_ReturnCurrency_When_MatchExists(){

        Mockito.when(currencyRepository.getByType("BGN"))
                .thenReturn(new Currency(1,"BGN"));

        Currency currency = currencyService.getByType("BGN");

        Assertions.assertEquals(1,currency.getId());
        Assertions.assertEquals("BGN",currency.getType());
    }

    @Test
    public void getByType_Should_Throw_When_MatchDoesNotExists(){
        Mockito.when(currencyRepository.getByType("BGN"))
                .thenThrow(new EntityNotFoundException("Currency","type","BGN"));

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> currencyService.getByType("BGN"));
    }
}
