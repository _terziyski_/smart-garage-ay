package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.helpers.CredentialsHelper;
import com.telerikacademy.smartgarage.helpers.CustomerHelper;
import com.telerikacademy.smartgarage.helpers.VehicleHelper;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Currency;
import com.telerikacademy.smartgarage.models.Employee;
import com.telerikacademy.smartgarage.models.Vehicle;
import com.telerikacademy.smartgarage.models.dtos.VehicleFilterParameters;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.smartgarage.helpers.CredentialsHelper.createMockCredentialsCustomer;
import static com.telerikacademy.smartgarage.helpers.CredentialsHelper.createMockCredentialsEmployee;
import static com.telerikacademy.smartgarage.helpers.VehicleHelper.createMockVehicle;
import static com.telerikacademy.smartgarage.helpers.VehicleHelper.createVfp;

@ExtendWith(MockitoExtension.class)
public class VehicleServiceImplTests {

    @Mock
    VehicleRepository vehicleRepository;

    @InjectMocks
    VehicleServiceImpl vehicleService;

    @Test
    public void getAll_Should_ReturnVehicles_When_MatchExists() {
        List<Vehicle> vehicles = new ArrayList<>();
        Vehicle vehicle = createMockVehicle();
        vehicles.add(vehicle);
        vehicle.setId(2);
        vehicles.add(vehicle);

        Mockito.when(vehicleRepository.getAll())
                .thenReturn(vehicles);

        List<Vehicle> result = vehicleService.getAll(createMockCredentialsEmployee());

        Assertions.assertEquals(vehicles, result);
    }

    @Test
    public void getAll_Should_Throw_When_UserIsNotEmployee() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> vehicleService.getAll(createMockCredentialsCustomer()));
    }

    @Test
    public void getAllByCustomerId_Should_ReturnVehicles_When_MatchExists() {
        List<Vehicle> vehicles = new ArrayList<>();
        Vehicle vehicle = createMockVehicle();
        Vehicle vehicle2 = createMockVehicle();
        vehicle2.setId(2);
        vehicles.add(vehicle);
        vehicles.add(vehicle2);

        Mockito.when(vehicleRepository.getAllByCustomerId(vehicle.getCustomer().getId()))
                .thenReturn(vehicles);

        List<Vehicle> result = vehicleService.getAllByCustomerId(vehicle.getCustomer().getId(),
                createMockCredentialsEmployee());

        Assertions.assertEquals(vehicles, result);
    }

    @Test
    public void getAllCustomerId_Should_Throw_When_UserIsNotEmployee() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> vehicleService.getAllByCustomerId(1, createMockCredentialsCustomer()));
    }


    @Test
    public void getAllByCredentialsId_Should_ReturnVehicles_When_MatchExists() {
        List<Vehicle> vehicles = new ArrayList<>();
        Vehicle vehicle = createMockVehicle();
        Vehicle vehicle2 = createMockVehicle();
        vehicle2.setId(2);
        vehicles.add(vehicle);
        vehicles.add(vehicle2);

        Mockito.when(vehicleRepository.getAllByCredentialsId(vehicle.getCustomer().getCredentials().getId()))
                .thenReturn(vehicles);

        List<Vehicle> result = vehicleService.getAllByCredentialsId(vehicle.getCustomer().getCredentials().getId(),
                createMockCredentialsEmployee());

        Assertions.assertEquals(vehicles, result);
    }


    @Test
    public void getAllCredentialsId_Should_Throw_When_UserIsNotOwner() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> vehicleService.getAllByCredentialsId(createMockCredentialsCustomer().getId() + 1,
                        createMockCredentialsCustomer()));
    }


    @Test
    public void getById_Should_ReturnVehicle_When_MatchExist() {
        // Arrange
        Vehicle vehicle = createMockVehicle();
        Mockito.when(vehicleRepository.getById(1))
                .thenReturn(vehicle);
        // Act
        Vehicle result = vehicleService.getById(1, createMockCredentialsEmployee());

        // Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals(vehicle.getCustomer(), result.getCustomer());
        Assertions.assertEquals(vehicle.getModel(), result.getModel());
        Assertions.assertEquals(vehicle.getLastSixVin(), result.getLastSixVin());
        Assertions.assertEquals(vehicle.getLicensePlate(), result.getLicensePlate());
        Assertions.assertEquals(vehicle.getVin(), result.getVin());
        Assertions.assertEquals(vehicle.getYear(), result.getYear());
    }

    @Test
    public void getById_Should_Throw_When_MatchDoesntExist() {
        // Arrange
        Mockito.when(vehicleRepository.getById(1))
                .thenThrow(new EntityNotFoundException("Vehicle", 1));

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> vehicleRepository.getById(1));
    }

    @Test
    public void getById_Should_Throw_When_UserIsNotEmployee() {
        //Arrange, Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> vehicleService.getById(1, createMockCredentialsCustomer()));
    }

    @Test
    public void create_ShouldCallRepository_When_Valid() {

        Assertions.assertThrows(DuplicateEntityException.class,() -> vehicleService.create(createMockVehicle(),createMockCredentialsEmployee()));

        Assertions.assertDoesNotThrow(
                () -> vehicleRepository.create(createMockVehicle()));
    }

    @Test
    public void create_Should_Throw_When_VehicleWithSameLicensePlateExists() {
        // Arrange
        Vehicle vehicle = createMockVehicle();

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> vehicleService.create(
                vehicle, createMockCredentialsEmployee()));
    }

    @Test
    public void create_Should_Throw_When_VehicleWithSameVinExists() {
        // Arrange
        Vehicle vehicle = createMockVehicle();
        vehicle.setLicensePlate("OO0000OO");

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> vehicleService.create(
                vehicle, createMockCredentialsEmployee()));
    }


    @Test
    public void create_Should_CallRepository_When_VehicleWithSameLicencePlateNotExist() {
        // Arrange
        Vehicle vehicle = createMockVehicle();

        Mockito.when(vehicleRepository.getByLicensePlate(vehicle.getLicensePlate()))
                .thenThrow(new EntityNotFoundException("Vehicle", "licence plate", vehicle.getLicensePlate()));


        Assertions.assertThrows(DuplicateEntityException.class,
                () -> vehicleService.create(vehicle, createMockCredentialsEmployee()));

    }


    @Test
    public void create_Should_CallRepository_When_VehicleWithSameVinNotExist() {
        // Arrange
        Vehicle vehicle = createMockVehicle();

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> vehicleService.create(vehicle, createMockCredentialsEmployee()));
    }

    @Test
    public void update_Should_CallRepository_When_VehicleWithSameLicencePlateNotExist() {
        // Arrange
        Vehicle vehicle = createMockVehicle();

        Mockito.when(vehicleRepository.getByLicensePlate(vehicle.getLicensePlate()))
                .thenThrow(new EntityNotFoundException("Vehicle", "licence plate", vehicle.getLicensePlate()));


        Assertions.assertThrows(DuplicateEntityException.class,
                () -> vehicleService.update(vehicle, createMockCredentialsEmployee()));

    }


    @Test
    public void update_Should_CallRepository_When_VehicleWithSameVinNotExist() {
        // Arrange
        Vehicle vehicle = createMockVehicle();

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> vehicleService.update(vehicle, createMockCredentialsEmployee()));
    }


    @Test
    public void delete_Should_CallRepository() {
        //Arrange
        Vehicle vehicle = createMockVehicle();

        //Act
        vehicleService.delete(vehicle, createMockCredentialsEmployee());

        //Assert
        Mockito.verify(vehicleRepository, Mockito.times(1))
                .delete(vehicle);
    }

    @Test
    public void delete_Should_Throw_When_UserIsNotEmployee() {
        //Arrange, Act, Assert
        Vehicle vehicle = createMockVehicle();
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> vehicleService.delete(vehicle, createMockCredentialsCustomer()));
    }

    @Test
    public void filter_Should_CallRepository() {
        //Arrange
        //Act
        vehicleService.filter(1, createMockCredentialsEmployee());

        //Assert
        Mockito.verify(vehicleRepository, Mockito.times(1))
                .filter(1);
    }

    @Test
    public void filter_Should_Throw_When_UserIsNotEmployee() {
        //Arrange, Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> vehicleService.filter(1, createMockCredentialsCustomer()));
    }


    @Test
    public void filterByNameOrEmail_Should_CallRepository() {
        //Arrange
        //Act

        Credentials credentials = createMockCredentialsEmployee();

        VehicleFilterParameters vfp = createVfp();
        vehicleService.filterByNameOrEmail(vfp, credentials);

        //Assert
        Mockito.verify(vehicleRepository, Mockito.times(1))
                .filterByNameOrEmail(vfp, credentials);
    }


}
