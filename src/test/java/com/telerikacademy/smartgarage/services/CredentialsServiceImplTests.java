package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.dtos.ChangePasswordDTO;
import com.telerikacademy.smartgarage.models.dtos.ForgotPasswordDTO;
import com.telerikacademy.smartgarage.repositories.contracts.CredentialsRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.smartgarage.helpers.CredentialsHelper.createMockCredentialsCustomer;

@ExtendWith(MockitoExtension.class)
public class CredentialsServiceImplTests {

    @Mock
    CredentialsRepository credentialsRepository;

    @Mock
    HttpSession session;

    @Mock
    JavaMailSender sender;

    @InjectMocks
    CredentialsServiceImpl credentialsService;

    @Test
    public void getAll_Should_ReturnCredentials_When_MatchExists(){
        List<Credentials> credentials = new ArrayList<>();
        credentials.add(createMockCredentialsCustomer());
        credentials.add(createMockCredentialsCustomer());

        Mockito.when(credentialsRepository.getAll())
                .thenReturn(credentials);

        List<Credentials> result = credentialsService.getAll();

        Assertions.assertEquals(credentials,result);
    }

    @Test
    public void getById_Should_ReturnCredentials_When_MatchExists(){

        Mockito.when(credentialsRepository.getById(1))
                .thenReturn(createMockCredentialsCustomer());

        Credentials credentials = credentialsService.getById(1);

        Assertions.assertEquals(1,credentials.getId());
        Assertions.assertEquals("mockCustomer@abv.bg",credentials.getEmail());
        Assertions.assertEquals("mockpass",credentials.getPassword());
    }

    @Test
    public void getById_Should_Throw_When_MatchDoesNotExists(){
        Mockito.when(credentialsRepository.getById(1))
                .thenThrow(new EntityNotFoundException("Credentials",1));

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> credentialsService.getById(1));
    }

    @Test
    public void getByEmail_Should_ReturnCredentials_When_MatchExists(){

        Mockito.when(credentialsRepository.getByEmail("mockCustomer@abv.bg"))
                .thenReturn(createMockCredentialsCustomer());

        Credentials credentials = credentialsService.getByEmail("mockCustomer@abv.bg");

        Assertions.assertEquals(1,credentials.getId());
        Assertions.assertEquals("mockCustomer@abv.bg",credentials.getEmail());
        Assertions.assertEquals("mockpass",credentials.getPassword());
    }

    @Test
    public void getByEmail_Should_Throw_When_MatchDoesNotExists(){
        Mockito.when(credentialsRepository.getByEmail("mockCustomer@abv.bg"))
                .thenThrow(new EntityNotFoundException("Credentials","email","mockCustomer@abv.bg"));

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> credentialsService.getByEmail("mockCustomer@abv.bg"));
    }

    @Test
    public void changePassword_Should_CallRepository_When_PasswordsMatches(){
        Credentials credentials = createMockCredentialsCustomer();
        ChangePasswordDTO dto = new ChangePasswordDTO();
        dto.setCurrentPassword("mockpass");
        dto.setNewPassword("mockpass2");
        dto.setRepeatNewPassword("mockpass2");

        Assertions.assertEquals(dto.getCurrentPassword(),credentials.getPassword());

        credentialsService.changePassword(dto,credentials);

        Assertions.assertEquals(dto.getNewPassword(),dto.getRepeatNewPassword());
        Assertions.assertEquals(credentials.getPassword(),dto.getNewPassword());
        Mockito.verify(credentialsRepository,Mockito.times(1)).changePassword(credentials);
    }

    @Test
    public void changePassword_Should_Throw_When_PasswordsDoesNotMatches(){
        Credentials credentials = createMockCredentialsCustomer();
        ChangePasswordDTO dto = new ChangePasswordDTO();
        dto.setCurrentPassword("mockpass");
        dto.setNewPassword("mockpass2");
        dto.setRepeatNewPassword("mockpass3");

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> credentialsService.changePassword(dto,credentials));
    }

    @Test
    public void forgotPassword_Should_CallRepository_When_PasswordMatches(){
        Credentials credentials = createMockCredentialsCustomer();
        ForgotPasswordDTO dto = new ForgotPasswordDTO();
        dto.setEmail("mockCustomer@abv.bg");
        dto.setNewPassword("mockpass2");
        dto.setRepeatNewPassword("mockpass2");

        Mockito.when(credentialsService.getByEmail(dto.getEmail()))
                .thenReturn(credentials);

        credentialsService.forgotPassword(dto);

        Assertions.assertEquals(credentials.getEmail(),dto.getEmail());
        Assertions.assertEquals(credentials.getPassword(),dto.getNewPassword());
        Mockito.verify(credentialsRepository,Mockito.times(1)).changePassword(credentials);
    }

    @Test
    public void forgotPassword_Should_Throw_When_PasswordsDoesNotMatch(){
        ForgotPasswordDTO dto = new ForgotPasswordDTO();
        dto.setEmail("mockCustomer@abv.bg");
        dto.setNewPassword("mockpass2");
        dto.setRepeatNewPassword("mockpass23");

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> credentialsService.forgotPassword(dto));
    }

    @Test
    public void sendLink_Should_Call_sendEmail() {
        String email = "mockEmail@abv.bg";
        Assertions.assertThrows(NullPointerException.class,() -> credentialsService.sendLink(email,session));
    }

    @Test
    public void sender_Should_CallSend(){
        MimeMessage message = sender.createMimeMessage();

        sender.send(message);

        Mockito.verify(sender,Mockito.times(1)).send(message);
    }
}
