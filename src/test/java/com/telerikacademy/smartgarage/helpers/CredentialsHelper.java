package com.telerikacademy.smartgarage.helpers;

import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Role;

import java.util.Set;

public class CredentialsHelper {
    public static Credentials createMockCredentialsCustomer(){
        Credentials customerCredentials = new Credentials();
        customerCredentials.setId(1);
        customerCredentials.setEmail("mockCustomer@abv.bg");
        customerCredentials.setPassword("mockpass");
        Role role = new Role();
        role.setType("customer");
        customerCredentials.setRole(Set.of(role));
        return customerCredentials;
    }

    public static Credentials createMockCredentialsEmployee(){
        Credentials employeeCredentials = new Credentials();
        employeeCredentials.setId(1);
        employeeCredentials.setEmail("mockEmployee@abv.bg");
        employeeCredentials.setPassword("mockpass");
        Role role = new Role();
        role.setType("employee");
        employeeCredentials.setRole(Set.of(role));
        return employeeCredentials;
    }
}
