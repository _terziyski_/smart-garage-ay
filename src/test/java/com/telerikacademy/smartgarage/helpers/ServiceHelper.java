package com.telerikacademy.smartgarage.helpers;

import com.telerikacademy.smartgarage.models.Service;

public class ServiceHelper {
    public static Service createMockService(){
        Service mockService = new Service();
        mockService.setId(1);
        mockService.setType("Mock Service");
        mockService.setDescription("Mock Description");
        mockService.setIcon("mock icon");
        mockService.setPrice(69.69);
        return mockService;
    }
}
