package com.telerikacademy.smartgarage.helpers;

import com.telerikacademy.smartgarage.models.Brand;
import com.telerikacademy.smartgarage.models.Model;
import com.telerikacademy.smartgarage.models.Vehicle;
import com.telerikacademy.smartgarage.models.dtos.VehicleFilterParameters;

import static com.telerikacademy.smartgarage.helpers.CustomerHelper.createMockCustomer;

public class VehicleHelper {
    public static Vehicle createMockVehicle(){
        Vehicle vehicle = new Vehicle();
        vehicle.setId(1);
        vehicle.setCustomer(createMockCustomer());
        vehicle.setLicensePlate("E5929BT");
        vehicle.setModel(createModel());
        vehicle.setVin("2139u120471289471290");
        vehicle.setYear(2019);
        return vehicle;
    }

    private static Model createModel(){
        Model model = new Model();
        model.setId(1);
        model.setName("GT63s");
        model.setBrand(createBrand());
        return model;
    }

    private static Brand createBrand(){
        Brand brand = new Brand();
        brand.setId(1);
        brand.setName("Mercedes-Benz");
        return brand;
    }

    public static VehicleFilterParameters createVfp() {
        VehicleFilterParameters vfp = new VehicleFilterParameters();
        vfp.setName("Rado");
        return vfp;
    }
}
