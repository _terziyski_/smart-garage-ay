package com.telerikacademy.smartgarage.helpers;

import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Customer;

import static com.telerikacademy.smartgarage.helpers.CredentialsHelper.createMockCredentialsCustomer;

public class CustomerHelper {
    public static Customer createMockCustomer(){
        Customer mockCustomer = new Customer();
        mockCustomer.setId(1);
        mockCustomer.setFirstName("Mock");
        mockCustomer.setLastName("Mockov");
        mockCustomer.setPhone("0883483939");
        mockCustomer.setCredentials(createMockCredentialsCustomer());
        return mockCustomer;
    }
}
