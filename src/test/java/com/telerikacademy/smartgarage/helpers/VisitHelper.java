package com.telerikacademy.smartgarage.helpers;

import com.telerikacademy.smartgarage.models.Visit;
import com.telerikacademy.smartgarage.models.dtos.VehicleFilterParameters;
import com.telerikacademy.smartgarage.models.dtos.VisitFilterParameters;

import java.sql.Date;
import java.util.Arrays;
import java.util.Set;

import static com.telerikacademy.smartgarage.helpers.ServiceHelper.createMockService;
import static com.telerikacademy.smartgarage.helpers.StatusHelper.createMockStatus;
import static com.telerikacademy.smartgarage.helpers.VehicleHelper.createMockVehicle;

public class VisitHelper {
    public static Visit createMockVisit(){
        Visit mockVisit = new Visit();
        mockVisit.setId(1);
        mockVisit.setDate(Date.valueOf("2021-04-16"));
        mockVisit.setServices(Set.of(createMockService()));
        mockVisit.setStatus(createMockStatus());
        mockVisit.setVehicle(createMockVehicle());
        mockVisit.setTotalPrice(69.69);
        return mockVisit;
    }

    public static VisitFilterParameters createVfp() {
        VisitFilterParameters vfp = new VisitFilterParameters();
        vfp.setLicensePlate(createMockVisit().getVehicle().getLicensePlate());
        vfp.setFromDate(Date.valueOf("2021-03-16"));
        vfp.setToDate(Date.valueOf("2021-04-16"));
        return vfp;
    }
}
