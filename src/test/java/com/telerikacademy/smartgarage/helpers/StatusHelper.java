package com.telerikacademy.smartgarage.helpers;

import com.telerikacademy.smartgarage.models.Status;

public class StatusHelper {
    public static Status createMockStatus(){
        Status status = new Status();
        status.setId(1);
        status.setType("Mock Status");
        return status;
    }
}
