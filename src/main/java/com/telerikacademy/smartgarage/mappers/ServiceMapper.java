package com.telerikacademy.smartgarage.mappers;

import com.telerikacademy.smartgarage.models.Service;
import com.telerikacademy.smartgarage.models.Visit;
import com.telerikacademy.smartgarage.models.dtos.ServiceDTO;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceRepository;
import com.telerikacademy.smartgarage.repositories.contracts.VisitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceMapper {
    private final VisitRepository visitRepository;
    private final ServiceRepository serviceRepository;

    @Autowired
    public ServiceMapper(VisitRepository visitRepository, ServiceRepository serviceRepository) {
        this.visitRepository = visitRepository;
        this.serviceRepository = serviceRepository;
    }

    public Service serviceFromDto(ServiceDTO serviceDTO){
        Service service = new Service();
        serviceDtoToObject(serviceDTO,service);
        return service;
    }
    public Service serviceFromDto(ServiceDTO serviceDTO,Integer id){
        Service service = serviceRepository.getById(id);
        serviceDtoToObject(serviceDTO,service);
        return service;
    }

    private void serviceDtoToObject(ServiceDTO serviceDTO, Service service) {
        service.setType(serviceDTO.getType());
        service.setPrice(serviceDTO.getPrice());
        service.setDescription(serviceDTO.getDescription());
        service.setIcon(serviceDTO.getIcon());
    }
}
