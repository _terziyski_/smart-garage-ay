package com.telerikacademy.smartgarage.mappers;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Brand;
import com.telerikacademy.smartgarage.models.Customer;
import com.telerikacademy.smartgarage.models.Model;
import com.telerikacademy.smartgarage.models.Vehicle;
import com.telerikacademy.smartgarage.models.dtos.VehicleDTO;
import com.telerikacademy.smartgarage.repositories.contracts.BrandRepository;
import com.telerikacademy.smartgarage.repositories.contracts.CustomerRepository;
import com.telerikacademy.smartgarage.repositories.contracts.ModelRepository;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class VehicleMapper {
    private final ModelRepository modelRepository;
    private final VehicleRepository vehicleRepository;
    private final CustomerRepository customerRepository;
    private final BrandRepository brandRepository;

    @Autowired
    public VehicleMapper(ModelRepository modelRepository,
                         VehicleRepository vehicleRepository,
                         CustomerRepository customerRepository,
                         BrandRepository brandRepository) {
        this.modelRepository = modelRepository;
        this.vehicleRepository = vehicleRepository;
        this.customerRepository = customerRepository;
        this.brandRepository = brandRepository;
    }

    public Vehicle vehicleFromDto(VehicleDTO vehicleDTO) {
        Vehicle vehicle = new Vehicle();
        vehicleDtoToObject(vehicleDTO, vehicle);
        return vehicle;
    }


    public Vehicle vehicleFromDto(VehicleDTO vehicleDTO, Integer id) {
        Vehicle vehicle = vehicleRepository.getById(id);
        vehicleDtoToObject(vehicleDTO, vehicle);
        return vehicle;
    }

    private void vehicleDtoToObject(VehicleDTO vehicleDTO, Vehicle vehicle) {
        Model model = setOrCreateModel(new Model(), new Brand(), vehicleDTO);
        Customer customer = customerRepository.getByEmail(vehicleDTO.getEmail());
        vehicle.setModel(model);
        vehicle.setYear(vehicleDTO.getYear());
        vehicle.setVin(vehicleDTO.getVin());
        vehicle.setLicensePlate(vehicleDTO.getLicensePlate());
        vehicle.setCustomer(customer);
    }

    private Model setOrCreateModel(Model model, Brand brand, VehicleDTO vehicleDTO) {
        List<Model> modelList = modelRepository.getAll();
        List<Brand> brandList = brandRepository.getAll();
        if (brandList.stream().noneMatch(o -> o.getName().equals(vehicleDTO.getBrand()))) {
            brand.setName(vehicleDTO.getBrand());
            brandRepository.create(brand);
        }
        if (modelList.stream().noneMatch(o -> o.getName().equals(vehicleDTO.getModel()))) {
            model.setBrand(brandRepository.getByName(vehicleDTO.getBrand()));
            model.setName(vehicleDTO.getModel());
            modelRepository.create(model);
        }
        model = modelRepository.getByName(vehicleDTO.getModel());
        return model;
    }
}
