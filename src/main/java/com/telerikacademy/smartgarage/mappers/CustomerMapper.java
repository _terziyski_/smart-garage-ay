package com.telerikacademy.smartgarage.mappers;

import com.telerikacademy.smartgarage.models.*;
import com.telerikacademy.smartgarage.models.dtos.CustomerDTO;
import com.telerikacademy.smartgarage.repositories.contracts.CustomerRepository;
import com.telerikacademy.smartgarage.repositories.contracts.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Random;
import java.util.Set;

@Component
public class CustomerMapper {

    private final CustomerRepository customerRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public CustomerMapper(CustomerRepository customerRepository, RoleRepository roleRepository) {
        this.customerRepository = customerRepository;
        this.roleRepository = roleRepository;
    }

    public Customer customerFromDto(CustomerDTO customerDTO) {
        Customer customer = new Customer();
        customerDtoToObject(customerDTO, customer);
        return customer;
    }

    public Customer customerFromDto(CustomerDTO customerDTO, int id) {
        Customer customer = customerRepository.getById(id);
        customerDtoToObject(customerDTO, customer);
        return customer;
    }

    private void customerDtoToObject(CustomerDTO customerDTO, Customer customer) {
        if(customerDTO.getEmail() != null){
            Credentials credentials = createCredentialsFromDTO(customerDTO);
            customer.setCredentials(credentials);
        }
        customer.setFirstName(customerDTO.getFirstName());
        customer.setLastName(customerDTO.getLastName());
        customer.setPhone(customerDTO.getPhone());
    }


    private Credentials createCredentialsFromDTO(CustomerDTO customerDTO) {
        Credentials credentials = new Credentials();
        Role role = roleRepository.getByName("customer");
        credentials.setRole(Set.of(role));
        credentials.setEmail(customerDTO.getEmail());
        credentials.setPassword(generatePassword());

        return credentials;
    }

    private String generatePassword(){
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }
}
