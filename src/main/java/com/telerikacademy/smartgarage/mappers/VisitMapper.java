package com.telerikacademy.smartgarage.mappers;

import com.telerikacademy.smartgarage.models.*;
import com.telerikacademy.smartgarage.models.dtos.VisitCreateDTO;
import com.telerikacademy.smartgarage.models.dtos.VisitDTO;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceRepository;
import com.telerikacademy.smartgarage.repositories.contracts.StatusRepository;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleRepository;
import com.telerikacademy.smartgarage.repositories.contracts.VisitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class VisitMapper {

    private final VisitRepository visitRepository;
    private final ServiceRepository serviceRepository;
    private final VehicleRepository vehicleRepository;
    private final StatusRepository statusRepository;

    @Autowired
    public VisitMapper(VehicleRepository vehicleRepository,
                       VisitRepository visitRepository,
                       ServiceRepository serviceRepository,
                       StatusRepository statusRepository) {
        this.vehicleRepository = vehicleRepository;
        this.visitRepository = visitRepository;
        this.serviceRepository = serviceRepository;
        this.statusRepository = statusRepository;
    }

    public Visit visitObjectFromDto(VisitCreateDTO visitCreateDTO) {
        Visit visit = new Visit();
        visitDtoToObject(visitCreateDTO, visit);
        return visit;
    }


    public Visit visitObjectFromDto(VisitDTO visitDTO) {
        Visit visit = new Visit();
        visitDtoToObject(visitDTO, visit);
        return visit;
    }

    public Visit visitObjectFromDto(VisitDTO visitDTO, Integer id) {
        Visit visit = visitRepository.getById(id);
        visitDtoToObject(visitDTO, visit);
        return visit;
    }

    public Visit visitObjectFromDto(VisitCreateDTO visitDTO, Integer id) {
        Visit visit = visitRepository.getById(id);
        visitDtoToObject(visitDTO, visit);
        return visit;
    }

    private void visitDtoToObject(VisitCreateDTO visitCreateDTO, Visit visit){
        Vehicle vehicle = vehicleRepository.getByLicensePlate(visitCreateDTO.getLicensePlate());
        visit.setVehicle(vehicle);
        visit.setDate(visitCreateDTO.getDate());
        visit.setTotalPrice(getServicesTotalPrice(visitCreateDTO));
        visit.setStatus(statusRepository.getById(visitCreateDTO.getStatusId()));
        visit.setServices(getServices(visitCreateDTO));
    }

    private void visitDtoToObject(VisitDTO visitDTO, Visit visit){
        Vehicle vehicle = vehicleRepository.getById(visitDTO.getVehicleId());
        visit.setVehicle(vehicle);
        visit.setDate(visitDTO.getDate());
        visit.setTotalPrice(visitDTO.getTotalPrice());
        visit.setStatus(statusRepository.getById(visitDTO.getStatusId()));
        visit.setServices(getServices(visitDTO));
    }

    private Set<Service> getServices(VisitDTO visitDTO){
        return visitDTO.getServicesIds()
                .stream()
                .map(serviceRepository::getById)
                .collect(Collectors.toSet());
    }

    private Set<Service> getServices(VisitCreateDTO visitCreateDTO){
        return visitCreateDTO.getServicesIds()
                .stream()
                .map(serviceRepository::getById)
                .collect(Collectors.toSet());
    }

    private Double getServicesTotalPrice(VisitCreateDTO visitCreateDTO){
        Double totalPrice = 0.0;
        for (int serviceId:visitCreateDTO.getServicesIds()) {
            totalPrice += serviceRepository.getById(serviceId).getPrice();
        }
        return totalPrice;
    }

}
