package com.telerikacademy.smartgarage.helpers;


import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.services.contracts.CredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

@Component
public class AuthenticationHelper {
    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";

    private final CredentialsService service;

    @Autowired
    public AuthenticationHelper(CredentialsService service) {
        this.service = service;
    }

    public Credentials tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "The requested resource requires authentication.");
        }

        try {
            String email = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return service.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid email!");
        }
    }

    public Credentials tryGetUser(HttpSession session) {
        String currentUserEmail = (String) session.getAttribute("currentUserEmail");

        if (currentUserEmail == null) {
            throw new UnauthorizedOperationException("No logged in user.");
        }

        try {
            return service.getByEmail(currentUserEmail);
        } catch (EntityNotFoundException e) {
            throw new UnauthorizedOperationException("No logged in user.");
        }
    }


    public void verifyAuthentication(String email, String password) {
        try {
            Credentials user = service.getByEmail(email);

            if (!user.getPassword().equals(password)) {
                throw new AuthenticationFailureException("Wrong username/password.");
            }

        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException("Wrong username/password.");
        }
    }


}
