package com.telerikacademy.smartgarage.helpers;


import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Credentials;

public class ValidationHelper {

    public static final String MODIFY_VEHICLES_BY_CUSTOMER_ERROR_MESSAGE = "Only employees can modify vehicles.";
    public static final String MODIFY_VISITS_BY_CUSTOMER_ERROR_MESSAGE = "Only employees can modify visits.";
    public static final String MODIFY_CUSTOMERS_BY_CUSTOMER_ERROR_MESSAGE = "Only employees can modify customers.";
    public static final String MODIFY_SERVICES_BY_CUSTOMER_ERROR_MESSAGE = "Only employees can modify services.";
    public static final String GET_VEHICLES_BY_CUSTOMER_ERROR_MESSAGE = "Only employees can get vehicles.";
    public static final String GET_VISITS_BY_CUSTOMER_ERROR_MESSAGE = "Only employees can get visits.";
    public static final String GET_VISITS_BY_LICENSEPLATE_ERROR_MESSAGE = "Incorrect Vehicle License Plate ";
    public static final String GET_CUSTOMERS_BY_CUSTOMER_ERROR_MESSAGE = "Only employees can get customers.";




    public static void checkIsEmployee(Credentials credentials, String msg) {
        if (!credentials.isEmployee()) {
            throw new UnauthorizedOperationException(msg);
        }
    }

    public static void checkIsCustomer(Credentials credentials, String msg) {
        if (!credentials.isCustomer()) {
            throw new UnauthorizedOperationException(msg);
        }
    }
}
