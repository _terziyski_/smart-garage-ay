package com.telerikacademy.smartgarage.services;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.telerikacademy.smartgarage.api.CurrencyConverterApi;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Customer;
import com.telerikacademy.smartgarage.models.Vehicle;
import com.telerikacademy.smartgarage.models.Visit;
import com.telerikacademy.smartgarage.models.dtos.VisitFilterParameters;
import com.telerikacademy.smartgarage.repositories.contracts.CustomerRepository;
import com.telerikacademy.smartgarage.repositories.contracts.VisitRepository;
import com.telerikacademy.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.FileOutputStream;
import java.sql.Date;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.telerikacademy.smartgarage.helpers.ValidationHelper.*;

@Service
public class VisitServiceImpl implements VisitService {

    private final JavaMailSender sender;
    private final VisitRepository visitRepository;
    private final CustomerRepository customerRepository;

    @Autowired
    public VisitServiceImpl(JavaMailSender sender,
                            VisitRepository visitRepository,
                            CustomerRepository customerRepository) {
        this.sender = sender;
        this.visitRepository = visitRepository;
        this.customerRepository = customerRepository;

    }

    @Override
    public List<Visit> getAll(Credentials credentials) {
        checkIsEmployee(credentials, GET_VISITS_BY_CUSTOMER_ERROR_MESSAGE);
        List<Visit> visits = visitRepository.getAll();
        visits.sort(Collections.reverseOrder());
        return visits;
    }

    @Override
    public Date getNewestVisitDate(Credentials credentials) {
        if (credentials.isCustomer()) {
            return visitRepository.getNewestVisitDateCustomer(credentials.getId());
        }
        return visitRepository.getNewestVisitDate();
    }

    @Override
    public Date getOldestVisitDate(Credentials credentials) {
        if (credentials.isCustomer()) {
            return visitRepository.getOldestVisitDateCustomer(credentials.getId());
        }
        return visitRepository.getOldestVisitDate();
    }

    @Override
    public Visit getById(int id, Credentials credentials) {
        int ownerCredentialsId = visitRepository.getById(id).getVehicle().getCustomer().getCredentials().getId();
        if (ownerCredentialsId != credentials.getId()) {
            checkIsEmployee(credentials, GET_VISITS_BY_CUSTOMER_ERROR_MESSAGE);
        }
        return visitRepository.getById(id);
    }

    @Override
    public List<Visit> getAllByCustomerId(Integer customerId, Credentials credentials) {
        Customer customer = customerRepository.getById(customerId);
        if (customer.getCredentials().getId() != credentials.getId()) {
            checkIsEmployee(credentials, GET_VISITS_BY_CUSTOMER_ERROR_MESSAGE);
        }
        List<Visit> visits = visitRepository.getAllByCustomerId(customerId);
        visits.sort(Collections.reverseOrder());
        return visits;
    }

    @Override
    public List<Visit> getAllByCredentialsId(Integer credentialsId) {
        List<Visit> visits = visitRepository.getAllByCredentialsId(credentialsId);
        visits.sort(Collections.reverseOrder());
        return visits;
    }

    @Override
    public List<Visit> filter(VisitFilterParameters vfp, Credentials credentials) {
        return visitRepository.filter(vfp, credentials);
    }

    @Override
    public void create(Visit visit, Credentials credentials) {
        checkIsEmployee(credentials, MODIFY_VISITS_BY_CUSTOMER_ERROR_MESSAGE);
        try {
            visitRepository.create(visit);
            sendEmail(visit);
        } catch (Exception e) {
            throw new UnsupportedOperationException("Error when trying to send email!");
        }
    }

    @Override
    public void update(Visit visit, Credentials credentials) {
        checkIsEmployee(credentials, MODIFY_VISITS_BY_CUSTOMER_ERROR_MESSAGE);
        visitRepository.update(visit);
    }

    @Override
    public void delete(Visit visit, Credentials credentials) {
        checkIsEmployee(credentials, MODIFY_VISITS_BY_CUSTOMER_ERROR_MESSAGE);
        visitRepository.delete(visit);
    }

    @Override
    public void generateReport(Visit visit, String currencyType, String email) throws Exception {
        Vehicle vehicle = visit.getVehicle();
        Customer customer = vehicle.getCustomer();
        Double currencyRate= CurrencyConverterApi.converter.rate("BGN", currencyType);
        Document document = new Document();
        String fileName = String.format("Visit-%d-Report.pdf", visit.getId());
        PdfWriter.getInstance(document, new FileOutputStream(fileName));

        document.open();
        Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
        Paragraph firstParagraph = new Paragraph();
        Chunk helloChunk = new Chunk(String.format("Hello %s,%n%nYou've been requested a report for a visit.%n" +
                "Here is everything about it.%n", customer.getFirstName()), font);
        firstParagraph.add(helloChunk);

        Set<com.telerikacademy.smartgarage.models.Service> serviceList = visit.getServices();
        StringBuilder services = new StringBuilder();

        for (com.telerikacademy.smartgarage.models.Service service : serviceList) {
            services.append(String.format("%s %s %s%n",
                    service.getType(), service.getTotalPriceFormatted(currencyRate), currencyType));
        }

        Paragraph secondParagraph = new Paragraph();
        Chunk infoChunk = new Chunk(String.format("%nVisit Date: %s%n%n" +
                        "Vehicle: %s %s%n%n" +
                        "Status: %s%n%n" +
                        "Services: %n%n%s%n%n",
                visit.getDate(), vehicle.getModel().getBrand().getName(), vehicle.getModel().getName(),
                visit.getStatus().getType(), services));
        Chunk totalPrice = new Chunk(String.format("Total price: %s %s",
                visit.getTotalPriceFormatted(currencyRate), currencyType), font);
        secondParagraph.add(infoChunk);
        secondParagraph.add(totalPrice);

        document.add(firstParagraph);
        document.add(secondParagraph);
        document.close();

        sendEmail(fileName, email);
    }

    public void sendEmail(Visit visit) throws Exception {
        Vehicle vehicle = visit.getVehicle();
        Customer customer = vehicle.getCustomer();

        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setTo(customer.getCredentials().getEmail());
        helper.setText(String.format("Hello %s,%nYou have visited our service on %s with your %s %s",
                customer.getFirstName(), visit.getDate(),
                vehicle.getModel().getBrand().getName(), vehicle.getModel().getName()));
        helper.setSubject("Your have visited our service");

        sender.send(message);
    }

    public void sendEmail(String fileName, String email) throws Exception {
        MimeMessage message = sender.createMimeMessage();
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
        message.setSubject("PDF Report");

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        Multipart multipart = new MimeMultipart();

        String file = String.format("C:/Users/juli0/Desktop/Smart_Garage/smart-garage-ay/%s", fileName);
        DataSource source = new FileDataSource(file);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(fileName);
        multipart.addBodyPart(messageBodyPart);
        message.setContent(multipart);
        sender.send(message);
    }

}
