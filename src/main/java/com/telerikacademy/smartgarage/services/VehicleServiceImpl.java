package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Vehicle;
import com.telerikacademy.smartgarage.models.dtos.VehicleFilterParameters;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleRepository;
import com.telerikacademy.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikacademy.smartgarage.helpers.ValidationHelper.*;

@Service
public class VehicleServiceImpl implements VehicleService {
    private final VehicleRepository vehicleRepository;

    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    @Override
    public List<Vehicle> getAll(Credentials credentials) {
        checkIsEmployee(credentials, GET_VEHICLES_BY_CUSTOMER_ERROR_MESSAGE);
        return vehicleRepository.getAll();
    }

    @Override
    public List<Vehicle> getAllByCustomerId(int customerId, Credentials credentials) {
        checkIsEmployee(credentials, GET_VEHICLES_BY_CUSTOMER_ERROR_MESSAGE);
        return vehicleRepository.getAllByCustomerId(customerId);
    }

    @Override
    public List<Vehicle> getAllByCredentialsId(int credentialsId, Credentials credentials) {
        if (credentialsId != credentials.getId()) {
            checkIsEmployee(credentials, GET_VEHICLES_BY_CUSTOMER_ERROR_MESSAGE);
        }
        return vehicleRepository.getAllByCredentialsId(credentialsId);
    }

    @Override
    public Vehicle getById(Integer id, Credentials credentials) {
        checkIsEmployee(credentials, GET_VEHICLES_BY_CUSTOMER_ERROR_MESSAGE);
        return vehicleRepository.getById(id);
    }

    @Override
    public List<Vehicle> filter(Integer customerId, Credentials credentials) {
        checkIsEmployee(credentials, GET_VEHICLES_BY_CUSTOMER_ERROR_MESSAGE);
        return vehicleRepository.filter(customerId);
    }

    @Override
    public List<Vehicle> filterByNameOrEmail(VehicleFilterParameters vfp, Credentials credentials) {
        return vehicleRepository.filterByNameOrEmail(vfp, credentials);
    }

    private void chekIfExistsByLicensePlate(Vehicle vehicle) {
        boolean duplicateExists = true;
        try {
            vehicleRepository.getByLicensePlate(vehicle.getLicensePlate());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Vehicle", "license plate", vehicle.getLicensePlate());
        }
    }

    private void chekIfExistsByVin(Vehicle vehicle) {
        boolean duplicateExists = true;
        try {
            vehicleRepository.getByVIN(vehicle.getVin());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Vehicle", "vin", vehicle.getVin());
        }
    }

    @Override
    public void create(Vehicle vehicle, Credentials credentials) {
        checkIsEmployee(credentials, MODIFY_VEHICLES_BY_CUSTOMER_ERROR_MESSAGE);
        chekIfExistsByLicensePlate(vehicle);
        chekIfExistsByVin(vehicle);
        vehicleRepository.create(vehicle);
    }

    @Override
    public void update(Vehicle vehicle, Credentials credentials) {
        checkIsEmployee(credentials, MODIFY_VEHICLES_BY_CUSTOMER_ERROR_MESSAGE);
        chekIfExistsByLicensePlate(vehicle);
        chekIfExistsByVin(vehicle);
        vehicleRepository.update(vehicle);
    }

    @Override
    public void delete(Vehicle vehicle, Credentials credentials) {
        checkIsEmployee(credentials, MODIFY_VEHICLES_BY_CUSTOMER_ERROR_MESSAGE);
        vehicleRepository.delete(vehicle);
    }
}
