package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.dtos.ChangePasswordDTO;
import com.telerikacademy.smartgarage.models.dtos.ForgotPasswordDTO;
import com.telerikacademy.smartgarage.repositories.contracts.CredentialsRepository;
import com.telerikacademy.smartgarage.services.contracts.CredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.UUID;

@Service
public class CredentialsServiceImpl implements CredentialsService {
    private final JavaMailSender sender;
    private final CredentialsRepository credentialsRepository;

    @Autowired
    public CredentialsServiceImpl(JavaMailSender sender,
                                  CredentialsRepository credentialsRepository) {
        this.sender = sender;
        this.credentialsRepository = credentialsRepository;
    }

    @Override
    public List<Credentials> getAll(){
        return credentialsRepository.getAll();
    }

    @Override
    public Credentials getById(int id){
        return credentialsRepository.getById(id);
    }

    @Override
    public Credentials getByEmail(String email){
        return credentialsRepository.getByEmail(email);
    }

    @Override
    public void changePassword(ChangePasswordDTO passwordDTO, Credentials credentials){
        String currentPassFromDto = passwordDTO.getCurrentPassword();
        String newPassword = passwordDTO.getNewPassword();
        String repeatedPassword = passwordDTO.getRepeatNewPassword();

        if (currentPassFromDto.equals(credentials.getPassword())
                && newPassword.equals(repeatedPassword)){
            credentials.setPassword(newPassword);
            credentialsRepository.changePassword(credentials);
        } else {
            throw new UnauthorizedOperationException("Error when trying to change your password!");
        }
    }

    @Override
    public void forgotPassword(ForgotPasswordDTO passwordDTO){
        String email = passwordDTO.getEmail();
        Credentials credentials = getByEmail(email);

        if (passwordDTO.getNewPassword().equals(passwordDTO.getRepeatNewPassword())){
            credentials.setPassword(passwordDTO.getNewPassword());
            credentialsRepository.changePassword(credentials);
        } else {
            throw new UnauthorizedOperationException("Error when trying to change your password!");
        }
    }

    @Override
    public void sendLink(String email, HttpSession session) throws Exception {
        String key = UUID.randomUUID().toString();
        sendEmail(email,key);
        session.setAttribute("key",key);
        session.setAttribute("email",email);
    }

    public void sendEmail(String email,String key) throws Exception {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setTo(email);
        helper.setText(String.format("Hello!%n" +
                "You forgot your password? No problem, click the link to change it!%n" +
                "http://localhost:8080/forgot-password/%s",key));
        helper.setSubject("Forgotten password");

        sender.send(message);
    }
}
