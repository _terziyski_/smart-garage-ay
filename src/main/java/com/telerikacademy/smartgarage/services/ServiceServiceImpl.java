package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.dtos.ServiceFilterParameters;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceRepository;
import com.telerikacademy.smartgarage.services.contracts.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikacademy.smartgarage.helpers.ValidationHelper.MODIFY_SERVICES_BY_CUSTOMER_ERROR_MESSAGE;
import static com.telerikacademy.smartgarage.helpers.ValidationHelper.checkIsEmployee;

@Service
public class ServiceServiceImpl implements ServiceService {
    private final ServiceRepository serviceRepository;

    @Autowired
    public ServiceServiceImpl(ServiceRepository serviceRepository) {
        this.serviceRepository = serviceRepository;
    }

    @Override
    public List<com.telerikacademy.smartgarage.models.Service> getAll(){
        return serviceRepository.getAll();
    }

    @Override
    public com.telerikacademy.smartgarage.models.Service getById(Integer id){
        return serviceRepository.getById(id);
    }

    @Override
    public List<com.telerikacademy.smartgarage.models.Service> filter(ServiceFilterParameters sfp){
        return serviceRepository.filter(sfp);
    }

    @Override
    public void create(com.telerikacademy.smartgarage.models.Service service, Credentials credentials){
        checkIsEmployee(credentials,MODIFY_SERVICES_BY_CUSTOMER_ERROR_MESSAGE);
        serviceRepository.create(service);
    }

    @Override
    public void update(com.telerikacademy.smartgarage.models.Service service, Credentials credentials){
        checkIsEmployee(credentials,MODIFY_SERVICES_BY_CUSTOMER_ERROR_MESSAGE);
        serviceRepository.update(service);
    }

    @Override
    public void delete(com.telerikacademy.smartgarage.models.Service service, Credentials credentials){
        checkIsEmployee(credentials,MODIFY_SERVICES_BY_CUSTOMER_ERROR_MESSAGE);
        serviceRepository.delete(service);
    }
}
