package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.dtos.ChangePasswordDTO;
import com.telerikacademy.smartgarage.models.dtos.ForgotPasswordDTO;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface CredentialsService {
    List<Credentials> getAll();

    Credentials getById(int id);

    Credentials getByEmail(String email);

    void changePassword(ChangePasswordDTO passwordDTO, Credentials credentials);

    void forgotPassword(ForgotPasswordDTO passwordDTO);

    void sendLink(String email, HttpSession session) throws Exception;
}
