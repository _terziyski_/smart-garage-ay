package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Currency;

import java.util.List;

public interface CurrencyService {
    List<Currency> getAll();

    Currency getById(int id);

    Currency getByType(String type);
}
