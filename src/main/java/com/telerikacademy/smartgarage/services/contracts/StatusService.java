package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Status;

import java.util.List;

public interface StatusService {

    List<Status> getAll();

    Status getById(int id);
}
