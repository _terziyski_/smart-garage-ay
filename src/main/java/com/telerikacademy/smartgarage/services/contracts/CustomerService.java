package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Customer;
import com.telerikacademy.smartgarage.models.dtos.CustomerFilterParameters;

import java.util.List;

public interface CustomerService {
    List<Customer> getAll(Credentials credentials);

    List<Customer> getAllSorted(String sortedBy, String order);

    Customer getById(Integer id, Credentials credentials);

    Customer getByEmail(String email, Credentials credentials);

    List<Customer> filter(CustomerFilterParameters cfp, Credentials credentials);

    void create(Customer customer, Credentials credentials) throws Exception;

    void update(Customer customer, Credentials credentials);

    void delete(Customer customer, Credentials credentials);
}
