package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Visit;
import com.telerikacademy.smartgarage.models.dtos.VisitFilterParameters;

import java.sql.Date;
import java.util.List;

public interface VisitService {
    List<Visit> getAll(Credentials credentials);

    Date getNewestVisitDate(Credentials credentials);

    Date getOldestVisitDate(Credentials credentials);

    List<Visit> getAllByCustomerId(Integer customerId, Credentials credentials);

    List<Visit> getAllByCredentialsId(Integer credentialsId);

    Visit getById(int id, Credentials credentials);

    List<Visit> filter(VisitFilterParameters vfp, Credentials credentials);

    void create(Visit visit, Credentials credentials);

    void update(Visit visit, Credentials credentials);

    void delete(Visit visit, Credentials credentials);

    void generateReport(Visit visit, String currencyType, String email) throws Exception;
}
