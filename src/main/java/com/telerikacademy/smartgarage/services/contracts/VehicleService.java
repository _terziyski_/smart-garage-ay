package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Vehicle;
import com.telerikacademy.smartgarage.models.dtos.VehicleFilterParameters;

import java.util.List;

public interface VehicleService {
    List<Vehicle> getAll(Credentials credentials);

    List<Vehicle> getAllByCustomerId(int customerId, Credentials credentials);

    List<Vehicle> getAllByCredentialsId(int credentialsId, Credentials credentials);

    Vehicle getById(Integer id, Credentials credentials);

    List<Vehicle> filter(Integer customerId, Credentials credentials);

    List<Vehicle> filterByNameOrEmail(VehicleFilterParameters vfp, Credentials credentials);

    void create(Vehicle vehicle, Credentials credentials);

    void update(Vehicle vehicle, Credentials credentials);

    void delete(Vehicle vehicle, Credentials credentials);
}
