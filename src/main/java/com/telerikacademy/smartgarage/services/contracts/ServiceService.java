package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Service;
import com.telerikacademy.smartgarage.models.dtos.ServiceFilterParameters;

import java.util.List;

public interface ServiceService {
    List<Service> getAll();

    Service getById(Integer id);

    List<Service> filter(ServiceFilterParameters sfp);

    void create(Service service, Credentials credentials);

    void update(Service service, Credentials credentials);

    void delete(Service service, Credentials credentials);
}
