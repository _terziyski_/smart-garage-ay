package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Customer;
import com.telerikacademy.smartgarage.models.dtos.CustomerFilterParameters;
import com.telerikacademy.smartgarage.repositories.contracts.CredentialsRepository;
import com.telerikacademy.smartgarage.repositories.contracts.CustomerRepository;
import com.telerikacademy.smartgarage.services.contracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.util.List;

import static com.telerikacademy.smartgarage.helpers.ValidationHelper.*;

@Service
public class CustomerServiceImpl implements CustomerService {
    private final JavaMailSender sender;
    private final CustomerRepository customerRepository;
    private final CredentialsRepository credentialsRepository;

    @Autowired
    public CustomerServiceImpl(JavaMailSender sender,
                               CustomerRepository customerRepository,
                               CredentialsRepository credentialsRepository) {
        this.sender = sender;
        this.customerRepository = customerRepository;
        this.credentialsRepository = credentialsRepository;
    }

    @Override
    public List<Customer> getAll(Credentials credentials) {
        checkIsEmployee(credentials, GET_CUSTOMERS_BY_CUSTOMER_ERROR_MESSAGE);
        return customerRepository.getAll();
    }

    @Override
    public List<Customer> getAllSorted(String sortedBy, String order) {
        return customerRepository.getAllSorted(sortedBy, order);
    }

    @Override
    public Customer getById(Integer id, Credentials credentials) {
        checkIsEmployee(credentials, GET_CUSTOMERS_BY_CUSTOMER_ERROR_MESSAGE);
        return customerRepository.getById(id);

    }

    @Override
    public Customer getByEmail(String email, Credentials credentials) {
        if (credentials.getEmail().equals(email)) {
            return customerRepository.getByEmail(email);
        }
        checkIsEmployee(credentials, GET_CUSTOMERS_BY_CUSTOMER_ERROR_MESSAGE);
        return customerRepository.getByEmail(email);
    }

    @Override
    public List<Customer> filter(CustomerFilterParameters cfp, Credentials credentials) {
        checkIsEmployee(credentials, GET_CUSTOMERS_BY_CUSTOMER_ERROR_MESSAGE);
        return customerRepository.filter(cfp);
    }

    @Override
    public void create(Customer customer, Credentials credentials) throws Exception {
        checkIsEmployee(credentials, MODIFY_CUSTOMERS_BY_CUSTOMER_ERROR_MESSAGE);
        checkIfExists(customer);
        customerRepository.create(customer);
        sendEmail(customer);
    }

    @Override
    public void update(Customer customer, Credentials credentials) {
        if (!customer.getCredentials().getEmail().equals(credentials.getEmail())) {
            checkIsEmployee(credentials, MODIFY_CUSTOMERS_BY_CUSTOMER_ERROR_MESSAGE);
        }
        customerRepository.update(customer);

    }

    @Override
    public void delete(Customer customer, Credentials credentials) {
        checkIsEmployee(credentials, MODIFY_CUSTOMERS_BY_CUSTOMER_ERROR_MESSAGE);
        customerRepository.delete(customer);
    }

    private void sendEmail(Customer customer) throws Exception {
        Credentials credentials = customer.getCredentials();

        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setTo(customer.getCredentials().getEmail());
        helper.setText(String.format("Hello %s,%n" +
                        "You have been registered to our web application.%n" +
                        "There is your login information:%n" +
                        "Email: %s%n" +
                        "Password: %s%n",
                customer.getFirstName(), credentials.getEmail(), credentials.getPassword()));
        helper.setSubject("Login information");

        sender.send(message);
    }

    private void checkIfExists(Customer customer){
        Credentials credentials = customer.getCredentials();
        boolean duplicateExist = true;
        try {
            credentialsRepository.getByEmail(credentials.getEmail());
        } catch (EntityNotFoundException e){
            duplicateExist = false;
        }
        if (duplicateExist){
            throw new DuplicateEntityException("Customer","email",credentials.getEmail());
        }
    }
}
