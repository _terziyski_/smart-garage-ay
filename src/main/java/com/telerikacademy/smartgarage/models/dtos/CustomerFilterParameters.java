package com.telerikacademy.smartgarage.models.dtos;

import java.sql.Date;

public class CustomerFilterParameters {

    private String param;
    private Date fromDate;
    private Date toDate;

    public CustomerFilterParameters(String param, Date fromDate, Date toDate) {
        this.param = param;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public CustomerFilterParameters() {

    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }
}
