package com.telerikacademy.smartgarage.models.dtos;

import com.telerikacademy.smartgarage.models.Service;
import com.telerikacademy.smartgarage.models.Vehicle;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.Set;

public class VisitDTO {

    @NotNull
    private int vehicleId;

    @Positive
    private Double totalPrice;

    @NotNull
    private Date date;

    @NotNull
    private int statusId;

    @NotNull
    private Set<Integer> servicesIds;

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public Set<Integer> getServicesIds() {
        return servicesIds;
    }

    public void setServicesIds(Set<Integer> servicesIds) {
        this.servicesIds = servicesIds;
    }
}
