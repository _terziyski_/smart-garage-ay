package com.telerikacademy.smartgarage.models.dtos;

public class VehicleFilterDTO {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
