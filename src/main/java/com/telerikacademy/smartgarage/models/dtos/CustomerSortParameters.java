package com.telerikacademy.smartgarage.models.dtos;

public class CustomerSortParameters {
    private String param;

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }
}
