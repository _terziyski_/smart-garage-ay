package com.telerikacademy.smartgarage.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class ServiceDTO {
    @NotNull
    private String type;

    @Positive
    private Double price;

    @NotNull
    private String description;

    @NotNull
    private String icon;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
