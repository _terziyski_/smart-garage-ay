package com.telerikacademy.smartgarage.models.dtos;

public class CurrencyConvertDTO {

    private Integer currencyId;

    private String type;

    public CurrencyConvertDTO(Integer currencyId, String type) {
        this.currencyId = currencyId;
        this.type = type;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
