package com.telerikacademy.smartgarage.models.dtos;

public class VehicleFilterParameters {

    private String name;

    public VehicleFilterParameters(String name) {
        this.name = name;
    }

    public VehicleFilterParameters() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
