package com.telerikacademy.smartgarage.models.dtos;

import javax.validation.constraints.*;

public class VehicleDTO {
    @NotEmpty
    private String brand;

    @NotEmpty
    private String model;

    @Positive
    @Size(min = 4,max = 4)
    private int year;

    @NotEmpty
    @Size(min = 17,max = 17,message = "Vin must be exactly 17 characters long!")
    private String vin;

    @NotEmpty
    @Pattern(regexp = "[A-Z]{1,2}[0-9]{4}[A-Z]{2}",message = "Invalid license plate format!")
    private String licensePlate;

    @Email
    private String email;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
