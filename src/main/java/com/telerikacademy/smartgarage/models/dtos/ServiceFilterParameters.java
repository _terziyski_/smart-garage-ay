package com.telerikacademy.smartgarage.models.dtos;

public class ServiceFilterParameters {
    private String type;
    private Double minPrice;
    private Double maxPrice;

    public ServiceFilterParameters(String type, Double minPrice, Double maxPrice) {
        this.type = type;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
    }

    public ServiceFilterParameters() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Double minPrice) {
        this.minPrice = minPrice;
    }

    public Double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }
}
