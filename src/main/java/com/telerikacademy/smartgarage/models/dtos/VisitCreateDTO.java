package com.telerikacademy.smartgarage.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Set;

public class VisitCreateDTO {

    @NotNull
    private String licensePlate;

    @Positive
    private Double totalPrice;

    @NotNull
    private Date date;

    @NotNull
    private int statusId;

    @NotNull
    private Set<Integer> servicesIds;

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public Set<Integer> getServicesIds() {
        return servicesIds;
    }

    public void setServicesIds(Set<Integer> servicesIds) {
        this.servicesIds = servicesIds;
    }
}


