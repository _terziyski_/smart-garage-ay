package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.services.contracts.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeMvcController {
    private final ServiceService serviceService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public HomeMvcController(ServiceService serviceService,
                             AuthenticationHelper authenticationHelper) {
        this.serviceService = serviceService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String indexPage(HttpSession session, Model model) {
        try {
            Credentials currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
        }
        model.addAttribute("services",serviceService.getAll());
        model.addAttribute("randomInt",Math.random() % 6);
        return "index";
    }
}
