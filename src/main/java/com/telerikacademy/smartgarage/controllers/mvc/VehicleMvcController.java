package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.mappers.VehicleMapper;
import com.telerikacademy.smartgarage.models.*;
import com.telerikacademy.smartgarage.models.dtos.VehicleDTO;
import com.telerikacademy.smartgarage.models.dtos.VehicleFilterDTO;
import com.telerikacademy.smartgarage.models.dtos.VehicleFilterParameters;
import com.telerikacademy.smartgarage.services.contracts.CustomerService;
import com.telerikacademy.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/vehicles")
public class VehicleMvcController {

    private final VehicleService vehicleService;
    private final VehicleMapper vehicleMapper;
    private final CustomerService customerService;
    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public VehicleMvcController(VehicleService vehicleService,
                                VehicleMapper vehicleMapper,
                                CustomerService customerService, AuthenticationHelper authenticationHelper) {
        this.vehicleService = vehicleService;

        this.vehicleMapper = vehicleMapper;
        this.customerService = customerService;
        this.authenticationHelper = authenticationHelper;
    }


    @GetMapping
    public String getAll(Model model, HttpSession session) {
        Credentials currentUser = authenticationHelper.tryGetUser(session);
        if (currentUser.isEmployee()) {
            model.addAttribute("vehicles", vehicleService.getAll(currentUser));
        } else {
            model.addAttribute("vehicles", vehicleService.getAllByCredentialsId(currentUser.getId(), currentUser));
        }
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("vehicleSearchDto", new VehicleFilterDTO());
        model.addAttribute("vehicleDto", new VehicleDTO());
        model.addAttribute("isHidden", Boolean.FALSE);

        return "vehicles";
    }

    @GetMapping("/customerId/{id}")
    public String getByCustomerId(@PathVariable int id, Model model, HttpSession session) {
        Credentials currentUser = authenticationHelper.tryGetUser(session);
        Customer customer = customerService.getById(id, currentUser);
        VehicleDTO vehicleDTO = new VehicleDTO();
        vehicleDTO.setEmail(customer.getCredentials().getEmail());
        List<Vehicle> vehicles = new ArrayList<>(
                vehicleService.getAllByCustomerId(customer.getId(), currentUser));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("vehicles", vehicles);
        model.addAttribute("vehicleSearchDto", new VehicleFilterDTO());
        model.addAttribute("vehicleDto", vehicleDTO);
        model.addAttribute("isHidden", Boolean.TRUE);
        return "vehicles";
    }

    @PostMapping("/create")
    public String create(@Validated @ModelAttribute("vehicleDto") VehicleDTO dto,
                         HttpSession session) {
        Credentials credentials = authenticationHelper.tryGetUser(session);
        Vehicle vehicle = vehicleMapper.vehicleFromDto(dto);
        vehicleService.create(vehicle, credentials);
        return "redirect:/vehicles";
    }

    @GetMapping("/filter")
    public String handleGetVehicleSearch() {
        return "redirect:/vehicles";
    }

    @PostMapping("/filter")
    public String handleVehicleSearch(Model model, HttpSession session,
                                      @ModelAttribute("vehicleFilterDto") VehicleFilterDTO vehicleFilterDTO) {
        Credentials currentUser = authenticationHelper.tryGetUser(session);
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("vehicleDto", new VehicleDTO());
        model.addAttribute("vehicleSearchDto", vehicleFilterDTO);
        model.addAttribute("vehicles", vehicleService.filterByNameOrEmail(
                new VehicleFilterParameters(vehicleFilterDTO.getName()), currentUser));
        return "vehicles";
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable int id, @Valid @ModelAttribute("vehicleDto") VehicleDTO dto,
                         HttpSession session) {
        Credentials credentials = authenticationHelper.tryGetUser(session);
        Vehicle vehicle = vehicleMapper.vehicleFromDto(dto, id);
        vehicleService.update(vehicle, credentials);
        return "redirect:/vehicles";
    }

}
