package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.models.dtos.ForgotPasswordDTO;
import com.telerikacademy.smartgarage.models.dtos.LoginDTO;
import com.telerikacademy.smartgarage.services.contracts.CredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping
public class AuthenticationMvcController {
    private final CredentialsService credentialsService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public AuthenticationMvcController(CredentialsService credentialsService,
                                       AuthenticationHelper authenticationHelper) {
        this.credentialsService = credentialsService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model){
        model.addAttribute("loginDto",new LoginDTO());
        model.addAttribute("passwordDto",new ForgotPasswordDTO());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("loginDto") LoginDTO dto,
                              BindingResult bindingResult,
                              HttpSession session){
        if (bindingResult.hasErrors()) {
            return "redirect:/";
        }

        try {
            authenticationHelper.verifyAuthentication(dto.getEmail(), dto.getPassword());
            session.setAttribute("currentUserEmail", dto.getEmail());
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "redirect:/";
        }

        return "redirect:/";
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUserEmail");
        return "redirect:/";
    }

    @PostMapping("/forgot-password")
    public String sendEmail(@ModelAttribute("passwordDto") ForgotPasswordDTO dto, HttpSession session,Model model) throws Exception {
        credentialsService.sendLink(dto.getEmail(), session);
        model.addAttribute("loginDto",new LoginDTO());
        return "/login";
    }

    @GetMapping("/forgot-password/{key}")
    public String forgotView(@PathVariable String key, HttpSession session, Model model){
        if (session.getAttribute("key").equals(key)){
            model.addAttribute("passwordDto",new ForgotPasswordDTO());
            return "forgot-password";
        }
        return "redirect:/";
    }

    @PostMapping("/forgot-password/{key}")
    public String changePassword(@Valid @ModelAttribute("passwordDto")ForgotPasswordDTO dto,HttpSession session){
        String email = session.getAttribute("email").toString();
        dto.setEmail(email);
        credentialsService.forgotPassword(dto);
        session.setAttribute("currentUserEmail", email);
        return "redirect:/";
    }
}
