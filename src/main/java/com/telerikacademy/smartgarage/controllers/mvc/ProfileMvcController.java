package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.mappers.CustomerMapper;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Customer;
import com.telerikacademy.smartgarage.models.dtos.ChangePasswordDTO;
import com.telerikacademy.smartgarage.models.dtos.CustomerDTO;
import com.telerikacademy.smartgarage.services.contracts.CredentialsService;
import com.telerikacademy.smartgarage.services.contracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/profile")
public class ProfileMvcController {
    private final CustomerMapper customerMapper;
    private final CustomerService customerService;
    private final CredentialsService credentialsService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ProfileMvcController(CustomerMapper customerMapper,
                                CustomerService customerService,
                                CredentialsService credentialsService,
                                AuthenticationHelper authenticationHelper) {
        this.customerMapper = customerMapper;
        this.customerService = customerService;
        this.credentialsService = credentialsService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showProfilePage(Model model, HttpSession session){
        Credentials credentials = authenticationHelper.tryGetUser(session);
        Customer customer = customerService.getByEmail(credentials.getEmail(),credentials);
        model.addAttribute("customer",customer);
        model.addAttribute("currentUser",credentials);
        model.addAttribute("customerDto",new CustomerDTO());
        model.addAttribute("passwordDto",new ChangePasswordDTO());
        return "profile";
    }

    @PostMapping("/update")
    public String update(@Valid @ModelAttribute("customerDto") CustomerDTO dto,
                         HttpSession session){
        Credentials credentials = authenticationHelper.tryGetUser(session);
        Customer customer = customerService.getByEmail(credentials.getEmail(),credentials);
        Customer customerToUpdate = customerMapper.customerFromDto(dto,customer.getId());
        customerService.update(customerToUpdate,credentials);
        return "redirect:/profile";
    }

    @PostMapping("/password")
    public String changePassword(@Valid @ModelAttribute("passwordDto") ChangePasswordDTO dto,
                                 HttpSession session){
        Credentials credentials = authenticationHelper.tryGetUser(session);
        credentialsService.changePassword(dto,credentials);
        return "redirect:/profile";
    }
}
