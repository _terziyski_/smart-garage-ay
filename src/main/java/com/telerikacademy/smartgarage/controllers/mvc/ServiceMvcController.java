package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.mappers.ServiceMapper;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Service;
import com.telerikacademy.smartgarage.models.Visit;
import com.telerikacademy.smartgarage.models.dtos.ServiceDTO;
import com.telerikacademy.smartgarage.services.contracts.ServiceService;
import com.telerikacademy.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/services")
public class ServiceMvcController {

    private final VisitService visitService;
    private final ServiceMapper serviceMapper;
    private final ServiceService serviceService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ServiceMvcController(ServiceService serviceService,
                                VisitService visitService,
                                ServiceMapper serviceMapper,
                                AuthenticationHelper authenticationHelper) {
        this.serviceService = serviceService;
        this.visitService = visitService;
        this.serviceMapper = serviceMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String getAll(HttpSession session,Model model){
        Credentials currentUser = authenticationHelper.tryGetUser(session);
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("services", serviceService.getAll());
        model.addAttribute("visits",visitService.getAll(currentUser));
        model.addAttribute("serviceDTO",new ServiceDTO());
        model.addAttribute("isHidden", Boolean.FALSE);
        return "services";
    }

    @GetMapping("/visitId/{id}")
    public String getByVisitId(@PathVariable int id, Model model, HttpSession session) {
        Credentials currentUser = authenticationHelper.tryGetUser(session);
        Visit visit = visitService.getById(id, currentUser);
        List<Service> services = new ArrayList<>(visit.getServices());
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("services", services);
        model.addAttribute("serviceDTO",new ServiceDTO());
        model.addAttribute("isHidden", Boolean.TRUE);
        return "services";
    }

    @PostMapping("/create")
    public String create(@Valid @ModelAttribute("serviceDTO") ServiceDTO dto,
                         HttpSession session){
        Credentials currentUser = authenticationHelper.tryGetUser(session);
        Service service = serviceMapper.serviceFromDto(dto);
        serviceService.create(service,currentUser);
        return "redirect:/services";
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable int id, @Valid @ModelAttribute("serviceDTO") ServiceDTO dto,
                         HttpSession session){
        Credentials currentUser = authenticationHelper.tryGetUser(session);
        Service service = serviceMapper.serviceFromDto(dto,id);
        serviceService.update(service,currentUser);
        return "redirect:/services";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable int id,HttpSession session){
        Credentials currentUser = authenticationHelper.tryGetUser(session);
        Service service = serviceService.getById(id);
        serviceService.delete(service,currentUser);
        return "redirect:/services";
    }
}
