package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.api.CurrencyConverterApi;
import com.telerikacademy.smartgarage.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.mappers.VisitMapper;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Customer;
import com.telerikacademy.smartgarage.models.Visit;
import com.telerikacademy.smartgarage.models.dtos.*;
import com.telerikacademy.smartgarage.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/visits")
public class VisitsMvcController {
    private final VisitService visitService;
    private final StatusService statusService;
    private final ServiceService serviceService;
    private final CustomerService customerService;
    private final CurrencyService currencyService;
    private final VisitMapper visitMapper;
    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public VisitsMvcController(VisitService visitService, StatusService statusService, ServiceService serviceService, CustomerService customerService, CurrencyService currencyService, VisitMapper visitMapper, AuthenticationHelper authenticationHelper) {
        this.visitService = visitService;
        this.statusService = statusService;
        this.serviceService = serviceService;
        this.customerService = customerService;
        this.currencyService = currencyService;
        this.visitMapper = visitMapper;
        this.authenticationHelper = authenticationHelper;
    }


    @GetMapping
    public String getAll(Model model, HttpSession session) {
        Credentials currentUser = authenticationHelper.tryGetUser(session);
        VisitFilterDTO visitFilterDTO = VFDTOSetDates(currentUser);
        VisitCreateDTO visitCreateDTO = VCDTOSetDates();
        if (currentUser.isEmployee()) {
            model.addAttribute("visits", visitService.getAll(currentUser));
        } else {
            model.addAttribute("visits", visitService.getAllByCredentialsId(currentUser.getId()));
        }
        model.addAttribute("currentUser", currentUser);
        addAttributes(model, visitFilterDTO, visitCreateDTO);
        model.addAttribute("isHidden",Boolean.FALSE);
        return "visits";
    }

    @GetMapping("/customerId/{id}")
    public String getByCustomerId(@PathVariable int id, Model model, HttpSession session) {
        Credentials currentUser = authenticationHelper.tryGetUser(session);
        Customer customer = customerService.getById(id, currentUser);
        VisitFilterDTO visitFilterDTO = VFDTOSetDates(currentUser);
        VisitCreateDTO visitCreateDTO = VCDTOSetDates();
        List<Visit> visits = new ArrayList<>(
                visitService.getAllByCustomerId(customer.getId(), currentUser));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("visits", visits);
        addAttributes(model, visitFilterDTO, visitCreateDTO);
        model.addAttribute("isHidden",Boolean.TRUE);

        return "visits";
    }

    @PostMapping("/create")
    public String create(@Valid @ModelAttribute("visitDto") VisitCreateDTO dto,
                         HttpSession session) {
        Credentials credentials = authenticationHelper.tryGetUser(session);
        Visit visit = visitMapper.visitObjectFromDto(dto);
        visitService.create(visit, credentials);
        return "redirect:/visits";
    }

    @GetMapping("/filter")
    public String handleGetVisitFilter() {
        return "redirect:/visits";
    }

    @PostMapping("/filter")
    public String handleVisitFilter(Model model, HttpSession session,
                                    @ModelAttribute("visitFilterDto") VisitFilterDTO visitFilterDTO) {
        Credentials currentUser = authenticationHelper.tryGetUser(session);
        addAttributes(model, currentUser);
        model.addAttribute("currencyConvertDto", new CurrencyConvertDTO(1, "BGN"));
        model.addAttribute("visitFilterDto", visitFilterDTO);
        model.addAttribute("currencyRate", 1);
        model.addAttribute("visits", visitService.filter(
                new VisitFilterParameters(visitFilterDTO.getLicensePlate(),
                        visitFilterDTO.getFromDate(), visitFilterDTO.getToDate()), currentUser));
        return "visits";
    }

    @PostMapping("/convert")
    public String handleVisitsConvert(Model model, HttpSession session,
                                      @ModelAttribute("currencyConvertDto") CurrencyConvertDTO currencyConvertDTO) {
        Credentials currentUser = authenticationHelper.tryGetUser(session);
        currencyConvertDTO.setType(currencyService.getById(currencyConvertDTO.getCurrencyId()).getType());
        addAttributes(model, currentUser);
        model.addAttribute("visitFilterDto", new VisitFilterDTO());
        model.addAttribute("services", serviceService.getAll());
        model.addAttribute("currencyConvertDto", currencyConvertDTO);
        model.addAttribute("visits", visitService.getAll(currentUser));
        model.addAttribute("currencyRate", CurrencyConverterApi.converter.rate("BGN", currencyConvertDTO.getType()));
        return "visits";
    }

    @GetMapping("/report/{id}/{currencyType}")
    public String sendReport(@PathVariable int id, @PathVariable String currencyType, HttpSession session) throws Exception {
        Credentials credentials = authenticationHelper.tryGetUser(session);

        Visit visit = visitService.getById(id, credentials);
        Credentials customerCredentials = visit.getVehicle().getCustomer().getCredentials();
        visitService.generateReport(visit, currencyType, customerCredentials.getEmail());
        return "redirect:/visits";
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable int id,@Valid @ModelAttribute("visitDto") VisitCreateDTO dto,
                         HttpSession session) {
        Credentials credentials = authenticationHelper.tryGetUser(session);
        Visit visit = visitMapper.visitObjectFromDto(dto,id);
        visitService.update(visit, credentials);
        return "redirect:/visits";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable int id, HttpSession session) {
        Credentials credentials = authenticationHelper.tryGetUser(session);
        Visit visit = visitService.getById(id, credentials);
        visitService.delete(visit, credentials);
        return "redirect:/visits";
    }

    private void addAttributes(Model model, VisitFilterDTO visitFilterDTO, VisitCreateDTO visitCreateDTO) {
        model.addAttribute("statuses", statusService.getAll());
        model.addAttribute("currencies", currencyService.getAll());
        model.addAttribute("currencyConvertDto", new CurrencyConvertDTO(1, "BGN"));
        model.addAttribute("visitDto", visitCreateDTO);
        model.addAttribute("visitFilterDto", visitFilterDTO);
        model.addAttribute("services", serviceService.getAll());
        model.addAttribute("currencyRate", 1);
    }

    private void addAttributes(Model model, Credentials currentUser) {
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("visitDto", new VisitCreateDTO());
        model.addAttribute("currencies", currencyService.getAll());
    }

    private VisitFilterDTO VFDTOSetDates(Credentials currentUser) {
        VisitFilterDTO visitFilterDTO = new VisitFilterDTO();
        visitFilterDTO.setFromDate(visitService.getNewestVisitDate(currentUser));
        visitFilterDTO.setToDate(visitService.getOldestVisitDate(currentUser));
        return visitFilterDTO;
    }

    private VisitCreateDTO VCDTOSetDates() {
        VisitCreateDTO visitCreateDTO = new VisitCreateDTO();
        visitCreateDTO.setDate(Date.valueOf(LocalDate.now()));
        return visitCreateDTO;
    }

}
