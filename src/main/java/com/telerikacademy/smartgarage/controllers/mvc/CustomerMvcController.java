package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.mappers.CustomerMapper;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Customer;
import com.telerikacademy.smartgarage.models.dtos.CustomerDTO;
import com.telerikacademy.smartgarage.models.dtos.CustomerFilterParameters;
import com.telerikacademy.smartgarage.models.dtos.CustomerSortParameters;
import com.telerikacademy.smartgarage.services.contracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/customers")
public class CustomerMvcController {
    private final CustomerMapper customerMapper;
    private final CustomerService customerService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CustomerMvcController(CustomerMapper customerMapper,
                                 CustomerService customerService,
                                 AuthenticationHelper authenticationHelper) {
        this.customerMapper = customerMapper;
        this.customerService = customerService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String getAll(Model model, HttpSession session) {
        Credentials currentUser = authenticationHelper.tryGetUser(session);
        CustomerFilterParameters cfp = new CustomerFilterParameters();
        cfp.setFromDate(Date.valueOf(LocalDate.now()));
        cfp.setToDate(Date.valueOf(LocalDate.now()));
        model.addAttribute("customers", customerService.getAll(currentUser));
        model.addAttribute("sort", new CustomerSortParameters());
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("customerDto", new CustomerDTO());
        model.addAttribute("filterParams", cfp);
        return "customers";
    }

    @GetMapping("/sort")
    public String getAllSorted() {
        return "redirect:/customers";
    }

    @PostMapping("/sort")
    public String sort(@ModelAttribute("sort") CustomerSortParameters sortBy,
                       Model model, HttpSession session) {
        Credentials currentUser = authenticationHelper.tryGetUser(session);
        CustomerFilterParameters cfp = new CustomerFilterParameters();
        cfp.setFromDate(Date.valueOf(LocalDate.now()));
        cfp.setToDate(Date.valueOf(LocalDate.now()));
        List<Customer> customers;
        switch (sortBy.getParam()) {
            case "name-asc":
                customers = customerService.getAllSorted("name", "asc");
                break;
            case "name-dsc":
                customers = customerService.getAllSorted("name", "dsc");
                break;
            case "date-asc":
                customers = customerService.getAllSorted("date", "asc");
                break;
            case "date-dsc":
                customers = customerService.getAllSorted("date", "dsc");
                break;
            default:
                customers = customerService.getAll(currentUser);
                break;
        }
        model.addAttribute("customers", customers);
        model.addAttribute("sort", sortBy);
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("customerDto", new CustomerDTO());
        model.addAttribute("filterParams",cfp);
        return "customers";
    }

    @GetMapping("/filter")
    public String filter() {
        return "redirect:/customers";
    }

    @PostMapping("/filter")
    public String handleFilter(@ModelAttribute("filterParams") CustomerFilterParameters cfp,
                               Model model, HttpSession session) {
        Credentials currentUser = authenticationHelper.tryGetUser(session);
        List<Customer> customers = customerService.filter(cfp, currentUser);
        model.addAttribute("customers", customers);
        model.addAttribute("sort", new CustomerSortParameters());
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("customerDto", new CustomerDTO());
        model.addAttribute("filterParams", cfp);
        return "customers";
    }

    @PostMapping("/create")
    public String create(@Valid @ModelAttribute("customerDto") CustomerDTO dto,
                         HttpSession session) throws Exception {
        Credentials credentials = authenticationHelper.tryGetUser(session);
        Customer customer = customerMapper.customerFromDto(dto);
        customerService.create(customer, credentials);
        return "redirect:/customers";
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable int id, @Valid @ModelAttribute("customerDto") CustomerDTO dto,
                         HttpSession session) {
        Credentials credentials = authenticationHelper.tryGetUser(session);
        Customer customer = customerMapper.customerFromDto(dto, id);
        customerService.update(customer, credentials);
        return "redirect:/customers";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable int id, HttpSession session) {
        Credentials credentials = authenticationHelper.tryGetUser(session);
        Customer customerToDelete = customerService.getById(id, credentials);
        customerService.delete(customerToDelete, credentials);
        return "redirect:/customers";
    }
}
