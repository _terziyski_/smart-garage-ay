package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.mappers.ServiceMapper;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Service;
import com.telerikacademy.smartgarage.models.dtos.ServiceDTO;
import com.telerikacademy.smartgarage.models.dtos.ServiceFilterParameters;
import com.telerikacademy.smartgarage.services.contracts.ServiceService;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/services")
public class ServiceRestController {
    private final ServiceService serviceService;
    private final ServiceMapper serviceMapper;
    private final AuthenticationHelper authenticationHelper;

    public ServiceRestController(ServiceService service,
                                 ServiceMapper serviceMapper,
                                 AuthenticationHelper authenticationHelper) {
        this.serviceService = service;
        this.serviceMapper = serviceMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Service> getAll(){
        return serviceService.getAll();
    }

    @GetMapping("/{id}")
    public Service getById(@PathVariable Integer id){
        return serviceService.getById(id);
    }

    @GetMapping("/filter")
    public List<Service> filter(@RequestParam(required = false) String type,
                                @RequestParam(required = false) Double minPrice,
                                @RequestParam(required = false) Double maxPrice){
        ServiceFilterParameters sfp = new ServiceFilterParameters(type,minPrice,maxPrice);
        return serviceService.filter(sfp);
    }

    @PostMapping
    public Service create(@Valid @RequestBody ServiceDTO serviceDTO,
                          HttpHeaders headers){
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        Service service = serviceMapper.serviceFromDto(serviceDTO);
        serviceService.create(service,credentials);
        return service;
    }

    @PutMapping("/{id}")
    public Service update(HttpHeaders headers,
                          @PathVariable Integer id,
                          @Valid @RequestBody ServiceDTO serviceDTO){
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        Service service = serviceMapper.serviceFromDto(serviceDTO,id);
        serviceService.update(service,credentials);
        return service;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id,
                       HttpHeaders headers){
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        Service service = serviceService.getById(id);
        serviceService.delete(service,credentials);
    }
}
