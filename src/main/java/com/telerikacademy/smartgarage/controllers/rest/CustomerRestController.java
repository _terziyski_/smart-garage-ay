package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.mappers.CustomerMapper;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Customer;
import com.telerikacademy.smartgarage.models.dtos.CustomerDTO;
import com.telerikacademy.smartgarage.models.dtos.CustomerFilterParameters;
import com.telerikacademy.smartgarage.services.contracts.CustomerService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;

import javax.validation.Valid;
import java.sql.Date;
import java.util.List;

@RestController
@RequestMapping("/api/customers")
@Api(value = "CustomersControllerAPI", produces = MediaType.APPLICATION_JSON_VALUE)
public class CustomerRestController {
    private final CustomerService customerService;
    private final CustomerMapper customerMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CustomerRestController(CustomerService customerService,
                                  CustomerMapper customerMapper,
                                  AuthenticationHelper authenticationHelper) {
        this.customerService = customerService;
        this.customerMapper = customerMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Customer> getAll(@RequestHeader HttpHeaders headers) {
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        return customerService.getAll(credentials);
    }

    @GetMapping("/{id}")
    public Customer getById(@RequestHeader HttpHeaders headers,
                            @PathVariable int id) {
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        return customerService.getById(id, credentials);
    }

    @GetMapping("/filter")
    public List<Customer> filter(@RequestHeader HttpHeaders headers,
                                 @RequestParam(required = false) String param,
                                 @RequestParam(required = false) Date fromDate,
                                 @RequestParam(required = false) Date toDate) {
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        CustomerFilterParameters cfp = new CustomerFilterParameters(param, fromDate, toDate);
        return customerService.filter(cfp, credentials);
    }

    @PostMapping
    public Customer create(@RequestHeader HttpHeaders headers,
                           @Valid @RequestBody CustomerDTO customerDTO) throws Exception {
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        Customer customer = customerMapper.customerFromDto(customerDTO);
        customerService.create(customer, credentials);

        return customer;

    }

    @PutMapping("/{id}")
    public Customer update(@RequestHeader HttpHeaders headers,
                           @PathVariable int id,
                           @Valid @RequestBody CustomerDTO customerDTO) {
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        Customer updatedCustomer = customerMapper.customerFromDto(customerDTO, id);
        customerService.update(updatedCustomer, credentials);
        return updatedCustomer;
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        Customer customer = customerService.getById(id, credentials);
        customerService.delete(customer, credentials);
    }
}


