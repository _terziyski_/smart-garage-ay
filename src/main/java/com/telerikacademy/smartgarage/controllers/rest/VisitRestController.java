package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.mappers.VisitMapper;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Visit;
import com.telerikacademy.smartgarage.models.dtos.VisitDTO;
import com.telerikacademy.smartgarage.models.dtos.VisitFilterParameters;
import com.telerikacademy.smartgarage.services.contracts.VisitService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Date;
import java.util.List;

@Controller
@RequestMapping("/api/visits")
@Api(value = "VisitControllerAPI", produces = MediaType.APPLICATION_JSON_VALUE)
public class VisitRestController {
    private final VisitService visitService;
    private final VisitMapper visitMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public VisitRestController(VisitService visitService, VisitMapper visitMapper, AuthenticationHelper authenticationHelper) {
        this.visitService = visitService;
        this.visitMapper = visitMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Visit> getAll(@RequestHeader HttpHeaders headers){
        Credentials credentials = authenticationHelper.tryGetUser(headers);

        return visitService.getAll(credentials);
    }

    @GetMapping("/{id}")
    public Visit getById(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        Credentials credentials = authenticationHelper.tryGetUser(headers);

        return visitService.getById(id, credentials);
    }

    @GetMapping("/filter")
    public List<Visit> filter(@RequestHeader HttpHeaders headers,
                              @RequestParam(required = false) String name,
                              @RequestParam(required = false) Date fromDate,
                              @RequestParam(required = false) Date toDate){
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        VisitFilterParameters vfp = new VisitFilterParameters(name, fromDate,toDate);
        return visitService.filter(vfp, credentials);
    }

    @PostMapping
    public Visit create(@Valid @RequestBody VisitDTO visitDTO, @RequestHeader HttpHeaders headers){
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        Visit visit = visitMapper.visitObjectFromDto(visitDTO);
        visitService.create(visit, credentials);
        return visit;

    }

    @PutMapping("/{id}")
    public Visit update(@Valid @PathVariable int id, @RequestBody VisitDTO visitDTO, @RequestHeader HttpHeaders headers) {
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        Visit updatedVisit = visitMapper.visitObjectFromDto(visitDTO, id);
        visitService.update(updatedVisit, credentials);
        return updatedVisit;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        Visit visit = visitService.getById(id, credentials);
        visitService.delete(visit, credentials);
    }

}
