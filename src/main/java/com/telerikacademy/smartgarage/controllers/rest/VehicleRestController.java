package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.mappers.VehicleMapper;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Vehicle;
import com.telerikacademy.smartgarage.models.dtos.VehicleDTO;
import com.telerikacademy.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/vehicles")
public class VehicleRestController {
    private final VehicleMapper vehicleMapper;
    private final VehicleService vehicleService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public VehicleRestController(VehicleMapper vehicleMapper,
                                 VehicleService vehicleService,
                                 AuthenticationHelper authenticationHelper) {
        this.vehicleMapper = vehicleMapper;
        this.vehicleService = vehicleService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Vehicle> getAll(@RequestHeader HttpHeaders headers){
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        return vehicleService.getAll(credentials);
    }

    @GetMapping("/{id}")
    public Vehicle getById(@RequestHeader HttpHeaders headers,
                           @PathVariable Integer id){
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        return vehicleService.getById(id,credentials);
    }

    @GetMapping("/filter")
    public List<Vehicle> filter(@RequestHeader HttpHeaders headers, @RequestParam(required = false) Integer customerId){
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        return vehicleService.filter(customerId, credentials);
    }

    @PostMapping
    public Vehicle create(@RequestHeader HttpHeaders headers,
                          @Valid @RequestBody VehicleDTO vehicleDTO){
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        Vehicle vehicle = vehicleMapper.vehicleFromDto(vehicleDTO);
        vehicleService.create(vehicle,credentials);
        return vehicle;
    }

    @PutMapping("/{id}")
    public Vehicle update(@RequestHeader HttpHeaders headers,
                          @PathVariable Integer id, @Valid @RequestBody VehicleDTO vehicleDTO){
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        Vehicle vehicle = vehicleMapper.vehicleFromDto(vehicleDTO,id);
        vehicleService.update(vehicle,credentials);
        return vehicle;
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable Integer id){
        Credentials credentials = authenticationHelper.tryGetUser(headers);
        Vehicle vehicleToDelete = vehicleService.getById(id,credentials);
        vehicleService.delete(vehicleToDelete,credentials);
    }
}
