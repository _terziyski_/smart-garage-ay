package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Brand;
import com.telerikacademy.smartgarage.repositories.contracts.BrandRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BrandRepositoryImpl implements BrandRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public BrandRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Brand> getAll(){
        try (Session session = sessionFactory.openSession()){
            Query<Brand> query = session.createQuery("from Brand ", Brand.class);
            return query.getResultList();
        }
    }

    @Override
    public Brand getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Brand brand = session.get(Brand.class, id);
            if (brand == null) {
                throw new EntityNotFoundException("Brand", id);
            }
            return brand;
        }
    }

    @Override
    public Brand getByName(String name){
        try (Session session = sessionFactory.openSession()){
            Query<Brand> query = session.createQuery("from Brand where name like :name", Brand.class);
            query.setParameter("name",name);
            List<Brand> result = query.getResultList();
            if (result.size() == 0){
                throw new EntityNotFoundException("Brand","name",name);
            }
            return result.get(0);
        }
    }

    @Override
    public void create(Brand brand) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(brand);
            session.getTransaction().commit();
        }
    }
}
