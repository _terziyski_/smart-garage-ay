package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Model;
import com.telerikacademy.smartgarage.repositories.contracts.ModelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ModelRepositoryImpl implements ModelRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public ModelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Model> getAll(){
        try (Session session = sessionFactory.openSession()){
            Query<Model> query = session.createQuery("from Model ", Model.class);
            return query.getResultList();
        }
    }

    @Override
    public Model getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Model model = session.get(Model.class, id);
            if (model == null) {
                throw new EntityNotFoundException("Model", id);
            }
            return model;
        }
    }

    @Override
    public Model getByName(String name){
        try (Session session = sessionFactory.openSession()){
            Query<Model> query = session.createQuery("from Model where name like :name", Model.class);
            query.setParameter("name",name);
            List<Model> result = query.getResultList();
            if (result.size() == 0){
                throw new EntityNotFoundException("Model","name",name);
            }
            return result.get(0);
        }
    }

    @Override
    public void create(Model model) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(model);
            session.getTransaction().commit();
        }
    }
}
