package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Visit;
import com.telerikacademy.smartgarage.models.dtos.VisitFilterParameters;
import com.telerikacademy.smartgarage.repositories.contracts.VisitRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public class VisitRepositoryImpl implements VisitRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public VisitRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Visit> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery("from Visit ", Visit.class);
            return query.getResultList();
        }
    }

    @Override
    public Date getNewestVisitDate() {
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery("from Visit order by date asc ", Visit.class);
            return query.getResultList().get(0).getDate();
        }
    }
    @Override
    public Date getNewestVisitDateCustomer(Integer credentialsId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery("from Visit as v " +
                    "where v.vehicle.customer.credentials.id = :credentialsId order by date asc ", Visit.class);
            query.setParameter("credentialsId", credentialsId);
            return query.getResultList().get(0).getDate();
        }
    }

    @Override
    public Date getOldestVisitDate() {
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery("from Visit order by date desc ", Visit.class);
            return query.getResultList().get(0).getDate();
        }
    }
    @Override
    public Date getOldestVisitDateCustomer(Integer credentialsId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery("from Visit as v " +
                    "where v.vehicle.customer.credentials.id = :credentialsId order by date desc ", Visit.class);
            query.setParameter("credentialsId", credentialsId);
            return query.getResultList().get(0).getDate();
        }
    }

    public List<Visit> getAllByCustomerId(Integer customerId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery("from Visit as v" +
                    " where v.vehicle.customer.id = :customerId ", Visit.class);
            query.setParameter("customerId", customerId);
            return query.getResultList();
        }
    }

    public List<Visit> getAllByCredentialsId(Integer credentialsId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery("from Visit as v" +
                    " where v.vehicle.customer.credentials.id = :credentialsId ", Visit.class);
            query.setParameter("credentialsId", credentialsId);
            return query.getResultList();
        }
    }

    @Override
    public Visit getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Visit visit = session.get(Visit.class, id);
            if (visit == null) {
                throw new EntityNotFoundException("Visit", id);
            }
            return visit;
        }
    }

    @Override
    public List<Visit> filter(VisitFilterParameters vfp, Credentials credentials) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder baseQuery = new StringBuilder("from Visit as v where 1=1 ");
            if(credentials.isCustomer()) {
                baseQuery.append("and v.vehicle.customer.credentials.id = :credentialsId ");
            }

            if (!vfp.getLicensePlate().isBlank()) {
                baseQuery.append("and v.vehicle.licensePlate like :licensePlate ");
            }
            if (vfp.getToDate() != null) {
                baseQuery.append("and v.date <= :toDate ");
            }
            if (vfp.getFromDate() != null) {
                baseQuery.append("and v.date >= :fromDate ");
            }
            Query<Visit> query = session.createQuery(baseQuery.toString(), Visit.class);

            if (credentials.isCustomer()) {
                query.setParameter("credentialsId", credentials.getId());
            }
            if (!vfp.getLicensePlate().isBlank()) {
                query.setParameter("licensePlate", vfp.getLicensePlate());
            }
            if (vfp.getToDate() != null) {
                query.setParameter("toDate", vfp.getToDate());
            }
            if (vfp.getFromDate() != null) {
                query.setParameter("fromDate", vfp.getFromDate());
            }
            return query.getResultList();
        }
    }

    @Override
    public void create(Visit visit) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(visit);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Visit visit) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(visit);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Visit visit) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(visit);
            session.getTransaction().commit();
        }
    }
}
