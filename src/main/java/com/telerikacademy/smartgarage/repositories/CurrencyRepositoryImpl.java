package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Currency;
import com.telerikacademy.smartgarage.repositories.contracts.CurrencyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CurrencyRepositoryImpl implements CurrencyRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CurrencyRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Currency> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Currency> query = session.createQuery("from Currency ", Currency.class);
            return query.getResultList();
        }
    }

    @Override
    public Currency getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Currency currency = session.get(Currency.class, id);
            if (currency == null) {
                throw new EntityNotFoundException("Currency", id);
            }
            return currency;
        }
    }

    @Override
    public Currency getByType(String type) {
        try (Session session = sessionFactory.openSession()) {
            Query<Currency> query = session.createQuery("from Currency where type = :type", Currency.class);
            query.setParameter("type", type);
            List<Currency> result = query.getResultList();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Currency", "type", type);
            }
            return result.get(0);
        }
    }
}
