package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Vehicle;
import com.telerikacademy.smartgarage.models.dtos.VehicleFilterParameters;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VehicleRepositoryImpl implements VehicleRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public VehicleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Vehicle> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery("from Vehicle ", Vehicle.class);
            return query.getResultList();
        }
    }

    @Override
    public List<Vehicle> getAllByCustomerId(int customerId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery("from Vehicle as v where v.customer.id = :customerId", Vehicle.class);
            query.setParameter("customerId", customerId);
            return query.getResultList();
        }
    }

    @Override
    public List<Vehicle> getAllByCredentialsId(int credentialsId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery("from Vehicle as v where v.customer.credentials.id = :credentialsId", Vehicle.class);
            query.setParameter("credentialsId", credentialsId);
            return query.getResultList();
        }
    }

    @Override
    public Vehicle getById(Integer id) {
        try (Session session = sessionFactory.openSession()) {
            Vehicle vehicle = session.get(Vehicle.class, id);
            if (vehicle == null) {
                throw new EntityNotFoundException("Vehicle", id);
            }
            return vehicle;
        }
    }

    @Override
    public Vehicle getByLicensePlate(String licensePlate) {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery("from Vehicle as v where  v.licensePlate = :licensePlate",
                    Vehicle.class);
            query.setParameter("licensePlate", licensePlate);
            List<Vehicle> result = query.getResultList();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Vehicle", licensePlate, "licensePlate");
            }
            return result.get(0);
        }
    }

    @Override
    public Vehicle getByVIN(String vin) {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery("from Vehicle as v where  v.vin = :vin",
                    Vehicle.class);
            query.setParameter("vin", vin);
            List<Vehicle> result = query.getResultList();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Vehicle", vin, "vin");
            }
            return result.get(0);
        }
    }

    @Override
    public List<Vehicle> filter(Integer customerId) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Vehicle where customer.id = :customerId",
                    Vehicle.class).getResultList();
        }
    }

    @Override
    public List<Vehicle> filterByNameOrEmail(VehicleFilterParameters vfp, Credentials credentials) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder baseQuery = new StringBuilder("from Vehicle as vh where 1=1 ");
            if(credentials.isCustomer()) {
                baseQuery.append("and vh.customer.credentials.id = :credentialsId ");
            }

            if (!vfp.getName().isBlank()) {
                baseQuery.append("and vh.customer.firstName like concat('%',:name,'%') ");
                baseQuery.append("or vh.customer.lastName like concat('%',:name,'%') ");
                baseQuery.append("or vh.customer.credentials.email like concat('%',:name,'%') ");
                baseQuery.append("or vh.licensePlate like concat('%',:name,'%') ");
            }

            Query<Vehicle> query = session.createQuery(baseQuery.toString(), Vehicle.class);

            if (credentials.isCustomer()) {
                query.setParameter("credentialsId", credentials.getId());
            }
            if (!vfp.getName().isBlank()) {
                query.setParameter("name", vfp.getName());
            }
            return query.getResultList();
        }
    }

    @Override
    public void create(Vehicle vehicle) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(vehicle);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Vehicle vehicle) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(vehicle);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Vehicle vehicle) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(vehicle);
            session.getTransaction().commit();
        }
    }
}
