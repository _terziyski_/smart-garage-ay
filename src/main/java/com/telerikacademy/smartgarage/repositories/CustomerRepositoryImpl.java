package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Customer;
import com.telerikacademy.smartgarage.models.Vehicle;
import com.telerikacademy.smartgarage.models.Visit;
import com.telerikacademy.smartgarage.models.dtos.CustomerFilterParameters;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CustomerRepositoryImpl implements com.telerikacademy.smartgarage.repositories.contracts.CustomerRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CustomerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Customer> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery("from Customer", Customer.class);
            return query.getResultList();
        }
    }

    @Override
    public List<Customer> getAllSorted(String sortedBy, String order) {
        try (Session session = sessionFactory.openSession()) {
            if (sortedBy.equals("date")) {
                Query<Visit> visitQuery;
                if (order.equals("asc")) {
                    visitQuery = session.createQuery("from Visit order by date", Visit.class);
                } else {
                    visitQuery = session.createQuery("from Visit order by date desc ", Visit.class);
                }
                List<Visit> visits = visitQuery.getResultList();
                List<Vehicle> vehicles = visits.stream().map(Visit::getVehicle).collect(Collectors.toList());
                return vehicles.stream().map(Vehicle::getCustomer).collect(Collectors.toList());
            } else {
                Query<Customer> query;
                if (order.equals("asc")) {
                    query = session.createQuery("from Customer order by firstName", Customer.class);
                } else {
                    query = session.createQuery("from Customer order by firstName desc ", Customer.class);
                }
                return query.getResultList();
            }
        }
    }

    @Override
    public Customer getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Customer customer = session.get(Customer.class, id);
            if (customer == null) {
                throw new EntityNotFoundException("Customer", id);
            }
            return customer;
        }
    }

    @Override
    public Customer getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery("from Customer as c where  c.credentials.email = :email",
                    Customer.class);
            query.setParameter("email", email);
            List<Customer> result = query.getResultList();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Customer", "email", email);
            }
            return result.get(0);
        }
    }

    @Override
    public void create(Customer customer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(customer);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Customer customer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(customer);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Customer customer) {
        try (Session session = sessionFactory.openSession()) {
            List<Vehicle> vehicles = session.createQuery("from Vehicle where customer.id = :id", Vehicle.class)
                    .setParameter("id", customer.getId())
                    .getResultList();
            List<Integer> ids = vehicles.stream().map(Vehicle::getId).collect(Collectors.toList());
            session.beginTransaction();
            for (Integer id : ids) {
                List<Visit> visits = session.createQuery("from Visit where vehicle.id = :id", Visit.class)
                        .setParameter("id", id)
                        .getResultList();
                if (visits.size() > 0)
                session.delete(visits.get(0));
            }
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Customer> filter(CustomerFilterParameters cfp) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder baseQueryCustomer = new StringBuilder("from Customer as c where 1=1 ");
            StringBuilder baseQueryVisit = new StringBuilder("from Visit as vs where 1=1 ");
            StringBuilder baseQueryVehicle = new StringBuilder("from Vehicle as vh where 1=1 ");


            if (cfp.getFromDate() != null || cfp.getToDate() != null) {
                if (cfp.getToDate() != null) {
                    baseQueryVisit.append("and vs.date <= :toDate ");
                }
                if (cfp.getFromDate() != null) {
                    baseQueryVisit.append("and vs.date >= :fromDate ");
                }
            }

            if (cfp.getParam() != null){
                baseQueryVehicle.append("or vh.model.brand.name like concat('%',:param,'%') ");
                baseQueryVehicle.append("or vh.model.name like concat('%',:param,'%') ");
                baseQueryCustomer.append("and concat(c.firstName,' ',c.lastName,' ',c.credentials.email,' ',c.phone) like concat('%',:param,'%') ");
            }

            Query<Customer> queryCustomer = session.createQuery(baseQueryCustomer.toString(), Customer.class);
            Query<Visit> queryVisit = session.createQuery(baseQueryVisit.toString(), Visit.class);
            Query<Vehicle> queryVehicle = session.createQuery(baseQueryVehicle.toString(), Vehicle.class);


            if (cfp.getParam() != null) {
                queryCustomer.setParameter("param", cfp.getParam());
                queryVehicle.setParameter("param", cfp.getParam());
            }

            if (cfp.getFromDate() != null || cfp.getToDate() != null){
                queryVisit.setParameter("fromDate",cfp.getFromDate());
                queryVisit.setParameter("toDate",cfp.getToDate());
            }

            return getResult(queryCustomer, queryVisit, queryVehicle);
        }
    }

    private List<Customer> getResult(Query<Customer> queryCustomer, Query<Visit> queryVisit, Query<Vehicle> queryVehicle) {
        List<Customer> customersFromCustomer = queryCustomer.getResultList();
        List<Customer> customersFromVisits;
        List<Customer> customersFromVehicles;

        List<Visit> visits = queryVisit.getResultList();
        List<Vehicle> vehicles = visits.stream().map(Visit::getVehicle).collect(Collectors.toList());
        customersFromVisits = vehicles.stream().map(Vehicle::getCustomer).collect(Collectors.toList());

        List<Vehicle> vehicleList = queryVehicle.getResultList();
        customersFromVehicles = vehicleList.stream().map(Vehicle::getCustomer).collect(Collectors.toList());

        int minSize = Math.min(Math.min(customersFromCustomer.size(), customersFromVisits.size())
                , customersFromVehicles.size());

        if (customersFromCustomer.size() == minSize){
            return customersFromCustomer;
        } else if (customersFromVehicles.size() == minSize){
            return customersFromVehicles;
        } else {
            return customersFromVisits;
        }
    }
}
