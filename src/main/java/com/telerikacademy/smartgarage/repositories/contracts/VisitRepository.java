package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Visit;
import com.telerikacademy.smartgarage.models.dtos.VisitFilterParameters;

import java.sql.Date;
import java.util.List;

public interface VisitRepository {
    List<Visit> getAll();

    Visit getById(int id);

    Date getNewestVisitDate();

    Date getOldestVisitDate();

    Date getNewestVisitDateCustomer(Integer credentialsId);

    Date getOldestVisitDateCustomer(Integer credentialsId);

    List<Visit> getAllByCustomerId(Integer customerId);

    List<Visit> getAllByCredentialsId(Integer customerId);

    List<Visit> filter(VisitFilterParameters vfp, Credentials credentials);

    void create(Visit visit);

    void update(Visit visit);

    void delete(Visit visit);
}
