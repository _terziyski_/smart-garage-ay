package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Credentials;

import java.util.List;

public interface CredentialsRepository {
    List<Credentials> getAll();

    Credentials getById(int id);

    Credentials getByEmail(String email);

    void changePassword(Credentials credentials);
}
