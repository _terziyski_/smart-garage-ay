package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Brand;

import java.util.List;

public interface BrandRepository {
    List<Brand> getAll();

    Brand getById(int id);

    Brand getByName(String name);

    void create(Brand brand);
}
