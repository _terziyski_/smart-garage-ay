package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Brand;
import com.telerikacademy.smartgarage.models.Model;

import java.util.List;

public interface ModelRepository {
    List<Model> getAll();

    Model getById(int id);

    Model getByName(String name);

    void create(Model model);
}
