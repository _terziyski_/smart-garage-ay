package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Service;
import com.telerikacademy.smartgarage.models.dtos.ServiceFilterParameters;

import java.util.List;

public interface ServiceRepository {
    List<Service> getAll();

    Service getById(Integer id);

    List<Service> filter(ServiceFilterParameters sfp);

    void create(Service service);

    void update(Service service);

    void delete(Service service);
}
