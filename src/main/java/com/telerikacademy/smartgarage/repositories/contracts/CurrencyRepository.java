package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Currency;

import java.util.List;

public interface CurrencyRepository {
    List<Currency> getAll();

    Currency getById(int id);

    Currency getByType(String type);
}
