package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Status;

import java.util.List;

public interface StatusRepository {

    List<Status> getAll();

    Status getById(int id);
}
