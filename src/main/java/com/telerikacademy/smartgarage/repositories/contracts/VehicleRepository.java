package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.models.Vehicle;
import com.telerikacademy.smartgarage.models.dtos.VehicleFilterParameters;

import java.util.List;

public interface VehicleRepository {
    List<Vehicle> getAll();

    List<Vehicle> getAllByCustomerId(int customerId);

    List<Vehicle> getAllByCredentialsId(int credentialsId);

    Vehicle getById(Integer id);

    Vehicle getByLicensePlate(String licensePlate);

    Vehicle getByVIN(String vin);

    List<Vehicle> filter(Integer customerId);

    List<Vehicle> filterByNameOrEmail(VehicleFilterParameters vfp, Credentials credentials);

    void create(Vehicle vehicle);

    void update(Vehicle vehicle);

    void delete(Vehicle vehicle);
}
