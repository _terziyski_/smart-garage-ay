package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Customer;
import com.telerikacademy.smartgarage.models.dtos.CustomerFilterParameters;

import java.util.List;

public interface CustomerRepository {
    List<Customer> getAll();

    List<Customer> getAllSorted(String sortedBy, String order);

    Customer getById(int id);

    Customer getByEmail(String email);

    List<Customer> filter(CustomerFilterParameters cfp);

    void create(Customer customer);

    void update(Customer customer);

    void delete(Customer customer);
}
