package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Service;
import com.telerikacademy.smartgarage.models.dtos.ServiceFilterParameters;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ServiceRepositoryImpl implements ServiceRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public ServiceRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Service> getAll(){
        try (Session session = sessionFactory.openSession()){
            Query<Service> query = session.createQuery("from Service ", Service.class);
            return query.getResultList();
        }
    }

    @Override
    public Service getById(Integer id) {
        try (Session session = sessionFactory.openSession()) {
            Service service = session.get(Service.class, id);
            if (service == null) {
                throw new EntityNotFoundException("Service", id);
            }
            return service;
        }
    }

    @Override
    public List<Service> filter(ServiceFilterParameters sfp){
        try (Session session = sessionFactory.openSession()){
            StringBuilder baseQuery = new StringBuilder("from Service as s where 1=1 ");
            List<String> filters = new ArrayList<>();

            if (!sfp.getType().isBlank()){
                filters.add("and s.type like :type ");
            }
            if (sfp.getMinPrice() != null && sfp.getMinPrice() != -1){
                filters.add("and s.price >= :minPrice ");
            }
            if (sfp.getMaxPrice() != null && sfp.getMaxPrice() != -1){
                filters.add("and s.price <= :minPrice ");
            }
            if (!filters.isEmpty()){
                for (String filter:filters) {
                    baseQuery.append(filter);
                }
            }
            Query<Service> query = session.createQuery(baseQuery.toString(),Service.class);
            if (!sfp.getType().isBlank()){
                query.setParameter("type",sfp.getType());
            }
            if (sfp.getMinPrice() != null && sfp.getMinPrice() != -1){
                query.setParameter("minPrice",sfp.getMinPrice());
            }
            if (sfp.getMaxPrice() != null && sfp.getMaxPrice() != -1){
                query.setParameter("maxPrice",sfp.getMaxPrice());
            }
            return query.getResultList();
        }
    }

    @Override
    public void create(Service service) {
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(service);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Service service){
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(service);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Service service){
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.delete(service);
            session.getTransaction().commit();
        }
    }
}
