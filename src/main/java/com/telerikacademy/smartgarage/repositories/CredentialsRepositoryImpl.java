package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Credentials;
import com.telerikacademy.smartgarage.repositories.contracts.CredentialsRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CredentialsRepositoryImpl implements CredentialsRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CredentialsRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Credentials> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Credentials> query = session.createQuery("from Credentials", Credentials.class);
            return query.getResultList();
        }
    }

    @Override
    public Credentials getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Credentials credentials = session.get(Credentials.class, id);
            if (credentials == null) {
                throw new EntityNotFoundException("Credentials", id);
            }
            return credentials;
        }
    }

    @Override
    public Credentials getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<Credentials> query = session.createQuery("from Credentials " +
                            "where email like :email",
                    Credentials.class);
            query.setParameter("email", email);
            List<Credentials> result = query.getResultList();

            if (result.size() == 0) {
                throw new EntityNotFoundException("Credentials", "email", email);
            }
            return result.get(0);
        }
    }

    @Override
    public void changePassword(Credentials credentials){
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(credentials);
            session.getTransaction().commit();
        }
    }
}
