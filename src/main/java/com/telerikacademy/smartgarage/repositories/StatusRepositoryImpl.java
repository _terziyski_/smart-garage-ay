package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Status;
import com.telerikacademy.smartgarage.repositories.contracts.StatusRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StatusRepositoryImpl implements StatusRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public StatusRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Status> getAll() {
        try (Session session = sessionFactory.openSession()){
            Query<Status> query = session.createQuery("from Status ", Status.class);
            return query.getResultList();
        }
    }

    @Override
    public Status getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Status status = session.get(Status.class, id);
            if (status == null) {
                throw new EntityNotFoundException("Status", id);
            }
            return status;
        }
    }
}
